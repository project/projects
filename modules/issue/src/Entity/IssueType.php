<?php

namespace Drupal\issue\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\issue\IssueTypeInterface;

/**
 * Defines the Issue type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "issue_type",
 *   label = @Translation("Issue type"),
 *   handlers = {
 *     "access" = "Drupal\issue\IssueTypeAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\issue\IssueTypeForm",
 *       "delete" = "Drupal\issue\Form\IssueTypeDeleteConfirm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\issue\IssueTypeListBuilder",
 *   },
 *   admin_permission = "administer issue types",
 *   config_prefix = "type",
 *   bundle_of = "issue",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   links = {
 *     "add-form" = "/admin/issue/type/add",
 *     "edit-form" = "/admin/issue/type/{issue_type}/edit",
 *     "delete-form" = "/admin/issue/type/{issue_type}/delete",
 *     "collection" = "/admin/issue/type",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *   }
 * )
 */
class IssueType extends ConfigEntityBundleBase implements IssueTypeInterface {

  /**
   * The machine name of this issue type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the issue type.
   *
   * @var string
   */
  protected $label;

  /**
   * A brief description of this issue type.
   *
   * @var string
   */
  protected $description;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

}
