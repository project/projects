<?php

namespace Drupal\issue;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a issue entity.
 */
interface IssueInterface extends ContentEntityInterface, EntityChangedInterface {

  const PRIORITY_HIGH = 30;
  const PRIORITY_MEDIUM = 20;
  const PRIORITY_LOW = 10;

}
