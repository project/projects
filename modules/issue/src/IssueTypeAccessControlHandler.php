<?php

namespace Drupal\issue;

use Drupal\Core\Entity\EntityAccessControlHandler;

/**
 * Defines the access control handler for the issue type entity type.
 *
 * @see \Drupal\issue\Entity\IssueType
 */
class IssueTypeAccessControlHandler extends EntityAccessControlHandler {

}
