<?php

namespace Drupal\issue\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\RendererInterface;
use Drupal\issue\IssueInterface;
use Drupal\issue\IssueTypeInterface;
use Drupal\cbo_project\ProjectInterface;
use Drupal\cbo_task\TaskInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns responses for Issue routes.
 */
class IssueController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a NodeController object.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   */
  public function __construct(RendererInterface $renderer) {
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('renderer')
    );
  }

  /**
   * Apply the workflow transition.
   */
  public function applyTransition(Request $request, IssueInterface $issue, $transition_id) {
    /** @var \Drupal\state_machine\Plugin\Field\FieldType\StateItem $state_item */
    $state_item = $issue->get('status')->first();
    $transitions = $state_item->getTransitions();
    if ($transition = $transitions[$transition_id]) {
      $state_item->applyTransition($transition);
      $issue->save();
    }

    if ($destination = $request->query->get('destination')) {
      return new RedirectResponse($destination);
    }
    else {
      return new RedirectResponse($issue->toUrl()->toString());
    }
  }

  /**
   * Provides add project issue page.
   */
  public function projectIssueAddPage(ProjectInterface $project) {
    $bundles = $this->entityManager()->getBundleInfo('issue');
    $build = [
      '#theme' => 'entity_add_list',
      '#bundles' => [],
    ];

    $link_text = $this->t('Add a new issue type.');
    $build['#add_bundle_message'] = $this->t('There is no @entity_type yet. @add_link', [
      '@entity_type' => $this->t('issue type'),
      '@add_link' => Link::createFromRoute($link_text, 'entity.issue_type.add_form')->toString(),
    ]);
    // Filter out the bundles the user doesn't have access to.
    $access_control_handler = $this->entityTypeManager()->getAccessControlHandler('issue');
    foreach ($bundles as $bundle_name => $bundle_info) {
      $access = $access_control_handler->createAccess($bundle_name, NULL, [], TRUE);
      if (!$access->isAllowed()) {
        unset($bundles[$bundle_name]);
      }
      $this->renderer->addCacheableDependency($build, $access);
    }

    // Redirect if there's only one bundle available.
    if (count($bundles) == 1) {
      $bundle_names = array_keys($bundles);
      return $this->redirect('issue.project_issue.add_form', [
        'project' => $project->id(),
        'issue_type' => reset($bundle_names),
      ]);
    }
    // Prepare the #bundles array for the template.
    foreach ($bundles as $bundle_name => $bundle_info) {
      $build['#bundles'][$bundle_name] = [
        'label' => $bundle_info['label'],
        'description' => isset($bundle_info['description']) ? $bundle_info['description'] : '',
        'add_link' => Link::createFromRoute(
          $bundle_info['label'],
          'issue.project_issue.add_form',
          ['project' => $project->id(), 'issue_type' => $bundle_name]
        ),
      ];
    }

    return $build;
  }

  /**
   * Provides add project issue form.
   */
  public function projectIssueAddForm(ProjectInterface $project, IssueTypeInterface $issue_type) {
    $issue = $this->entityManager()->getStorage('issue')->create([
      'project' => $project->id(),
      'type' => $issue_type->id(),
    ]);

    $form = $this->entityFormBuilder()->getForm($issue);

    return $form;
  }

  /**
   * Provides add task issue page.
   */
  public function taskIssueAddPage(TaskInterface $task) {
    $bundles = $this->entityManager()->getBundleInfo('issue');
    $build = [
      '#theme' => 'entity_add_list',
      '#bundles' => [],
    ];

    $link_text = $this->t('Add a new issue type.');
    $build['#add_bundle_message'] = $this->t('There is no @entity_type yet. @add_link', [
      '@entity_type' => $this->t('issue type'),
      '@add_link' => Link::createFromRoute($link_text, 'entity.issue_type.add_form')->toString(),
    ]);
    // Filter out the bundles the user doesn't have access to.
    $access_control_handler = $this->entityTypeManager()->getAccessControlHandler('issue');
    foreach ($bundles as $bundle_name => $bundle_info) {
      $access = $access_control_handler->createAccess($bundle_name, NULL, [], TRUE);
      if (!$access->isAllowed()) {
        unset($bundles[$bundle_name]);
      }
      $this->renderer->addCacheableDependency($build, $access);
    }

    // Redirect if there's only one bundle available.
    if (count($bundles) == 1) {
      $bundle_names = array_keys($bundles);
      return $this->redirect('issue.task_issue.add_form', [
        'task' => $task->id(),
        'issue_type' => reset($bundle_names),
      ]);
    }
    // Prepare the #bundles array for the template.
    foreach ($bundles as $bundle_name => $bundle_info) {
      $build['#bundles'][$bundle_name] = [
        'label' => $bundle_info['label'],
        'description' => isset($bundle_info['description']) ? $bundle_info['description'] : '',
        'add_link' => Link::createFromRoute(
          $bundle_info['label'],
          'issue.task_issue.add_form',
          ['task' => $task->id(), 'issue_type' => $bundle_name]
        ),
      ];
    }

    return $build;
  }

  /**
   * Provides add task issue form.
   */
  public function taskIssueAddForm(TaskInterface $task, IssueTypeInterface $issue_type) {
    $issue = $this->entityManager()->getStorage('issue')->create([
      'task' => $task->id(),
      'type' => $issue_type->id(),
    ]);

    $form = $this->entityFormBuilder()->getForm($issue);

    return $form;
  }

  /**
   * The _title_callback for the issue.add route.
   *
   * @param \Drupal\issue\IssueTypeInterface $issue_type
   *   The current issue type.
   *
   * @return string
   *   The page title
   */
  public function addPageTitle(IssueTypeInterface $issue_type) {
    return $this->t('Create @name', ['@name' => $issue_type->label()]);
  }

}
