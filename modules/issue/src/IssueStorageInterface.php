<?php

namespace Drupal\issue;

use Drupal\Core\Entity\ContentEntityStorageInterface;

/**
 * Defines an interface for issue entity storage classes.
 */
interface IssueStorageInterface extends ContentEntityStorageInterface {

}
