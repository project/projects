<?php

namespace Drupal\issue;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * Provides an interface defining a issue type entity.
 */
interface IssueTypeInterface extends ConfigEntityInterface, EntityDescriptionInterface {

}
