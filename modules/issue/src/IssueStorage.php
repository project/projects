<?php

namespace Drupal\issue;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * Defines the storage handler class for issue.
 */
class IssueStorage extends SqlContentEntityStorage implements IssueStorageInterface {

}
