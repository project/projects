<?php

namespace Drupal\issue;

use Drupal\views\EntityViewsData;

/**
 * Provides the views data for the issue entity type.
 */
class IssueViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['issue']['bulk_form'] = [
      'title' => $this->t('Operations bulk form'),
      'help' => $this->t('Add a form element that lets you run operations on multiple issues.'),
      'field' => [
        'id' => 'bulk_form',
      ],
    ];

    $data['issue']['project']['argument']['id'] = 'project';
    $data['issue']['task']['argument']['id'] = 'task';
    $data['issue']['status']['filter']['id'] = 'state_machine_state';

    return $data;
  }

}
