<?php

namespace Drupal\issue\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting a issue.
 */
class IssueDeleteForm extends ContentEntityDeleteForm {

}
