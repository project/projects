<?php

namespace Drupal\issue\Form;

use Drupal\Core\Entity\EntityDeleteForm;

/**
 * Provides a form for issue type deletion.
 */
class IssueTypeDeleteConfirm extends EntityDeleteForm {

}
