<?php

namespace Drupal\issue\Plugin\Action;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Action\ConfigurableActionBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\state_machine\WorkflowManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Apply transition to workflow.
 *
 * @Action(
 *   id = "issue_apply_transition",
 *   label = @Translation("Apply transition to the selected issues"),
 *   type = "issue"
 * )
 */
class ApplyTransition extends ConfigurableActionBase implements ContainerFactoryPluginInterface {

  /**
   * The workflow manager service.
   *
   * @var \Drupal\state_machine\WorkflowManagerInterface
   */
  protected $workflowManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, WorkflowManagerInterface $workflow_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->workflowManager = $workflow_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.workflow')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'transition' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\state_machine\Plugin\Workflow\WorkflowInterface $workflow */
    $workflow = $this->workflowManager->createInstance('issue_status');
    $transitions = $workflow->getTransitions();
    $options = array_map(function ($transition) {
      return $transition->getLabel();
    }, $transitions);
    $form['transition'] = [
      '#type' => 'radios',
      '#title' => t('Transition'),
      '#options' => $options,
      '#default_value' => $this->configuration['transition'],
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['transition'] = $form_state->getValue('transition');
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    /** @var \Drupal\issue\IssueInterface $object */
    $state_item = $object->get('status')->first();
    if (in_array($this->configuration['transition'], array_keys($state_item->getTransitions()))) {
      $access = $object->access('update', $account, TRUE)
        ->andIf($object->get('status')->access('edit', $account, TRUE));

      return $return_as_object ? $access : $access->isAllowed();
    }

    return $return_as_object ? AccessResult::forbidden() : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    /** @var \Drupal\state_machine\Plugin\Field\FieldType\StateItem $state_item */
    $state_item = $entity->get('status')->first();
    $transitions = $state_item->getTransitions();
    if ($transition = $transitions[$this->configuration['transition']]) {
      $state_item->applyTransition($transition);
      $entity->save();
    }
  }

}
