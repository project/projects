<?php

namespace Drupal\issue\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\issue\IssueInterface;
use Drupal\state_machine\WorkflowManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block to display Issues.
 *
 * @Block(
 *   id = "issues",
 *   admin_label = @Translation("Issues")
 * )
 */
class Issues extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The currently active route match object.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The workflow manager service.
   *
   * @var \Drupal\state_machine\WorkflowManagerInterface
   */
  protected $workflowManager;

  /**
   * Constructs a new SystemMenuBlock.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current active route match object.
   * @param \Drupal\state_machine\WorkflowManagerInterface $workflow_manager
   *   The workflow manager service.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, EntityTypeManagerInterface $entity_type_manager, RouteMatchInterface $route_match, WorkflowManagerInterface $workflow_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->routeMatch = $route_match;
    $this->workflowManager = $workflow_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('current_route_match'),
      $container->get('plugin.manager.workflow')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'priority' => 0,
      'issue_status' => '',
      'progress_status' => '',
      'count' => 0,
      'add_on_empty' => FALSE,
      'more_link' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->configuration;

    $form['priority'] = [
      '#type' => 'select',
      '#title' => $this->t('Priority'),
      '#default_value' => $config['priority'],
      '#options' => [
        0 => $this->t('-- Any --'),
        IssueInterface::PRIORITY_HIGH => $this->t('High'),
        IssueInterface::PRIORITY_MEDIUM => $this->t('Medium'),
        IssueInterface::PRIORITY_LOW => $this->t('Low'),
      ],
    ];

    $workflow = $this->workflowManager->createInstance('issue_status');
    $states = $workflow->getStates();
    $options = array_map(function ($state) {
      return $state->getLabel();
    }, $states);
    $options = [
      '' => $this->t('-- Any --'),
      'open' => $this->t('-- Open --'),
    ] + $options;
    $form['issue_status'] = [
      '#type' => 'select',
      '#title' => $this->t('Status'),
      '#default_value' => $config['issue_status'],
      '#options' => $options,
    ];

    $form['progress_status'] = [
      '#type' => 'select',
      '#title' => $this->t('Progress status'),
      '#default_value' => $config['progress_status'],
      '#options' => [
        '' => $this->t('-- Any --'),
        'on_track' => $this->t('On Track'),
        'at_risk' => $this->t('At Risk'),
        'in_trouble' => $this->t('In Trouble'),
      ],
    ];

    $form['count'] = [
      '#type' => 'number',
      '#title' => $this->t('Item count'),
      '#min' => 1,
      '#default_value' => $config['count'] > 0 ? $config['count'] : '',
      '#description' => $this->t('The item count to display.'),
    ];

    $form['add_on_empty'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add on empty'),
      '#defalut_value' => $config['add_on_empty'],
    ];

    $form['more_link'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display more link'),
      '#default_value' => $config['more_link'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['priority'] = $form_state->getValue('priority');
    $this->configuration['issue_status'] = $form_state->getValue('issue_status');
    $this->configuration['progress_status'] = $form_state->getValue('progress_status');
    $this->configuration['count'] = $form_state->getValue('count');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->configuration;

    $storage = $this->entityTypeManager->getStorage('issue');
    $query = $storage->getQuery();

    if (!empty($config['priority'])) {
      $query->condition('priority', $config['priority']);
    }
    if (!empty($config['issue_status'])) {
      if ($config['issue_status'] == 'open') {
        $query->condition('status', ['closed', 'canceled'], 'NOT IN');
      }
      else {
        $query->condition('status', $config['issue_status']);
      }
    }
    if (!empty($config['progress_status'])) {
      $query->condition('progress_status', $config['progress_status']);
    }

    if ($task = $this->routeMatch->getParameter('task')) {
      $query->condition('task', $task->id());
    }
    elseif ($workplan = $this->routeMatch->getParameter('workplan')) {
      $tasks = $this->entityTypeManager->getStorage('task')->getQuery()
        ->condition('workplan', $workplan->id())
        ->execute();
      if (!empty($tasks)) {
        $query->condition('task', $tasks, 'IN');
      }
    }
    elseif ($project = $this->routeMatch->getParameter('project')) {
      $query->condition('project', $project->id());
    }
    elseif ($organization = $this->routeMatch->getParameter('organization')) {
      $tasks = $this->entityTypeManager->getStorage('task')->getQuery()
        ->condition('organization', $organization->id())
        ->execute();
      if (!empty($tasks)) {
        $query->condition('task', $tasks, 'IN');
      }
    }

    if ($config['count'] > 0) {
      $query->range(NULL, $config['count']);
    }

    $ids = $query->sort('changed', 'DESC')
      ->execute();

    $items = [];
    if (!empty($ids)) {
      $entities = $storage->loadMultiple($ids);
      /** @var \Drupal\issue\IssueInterface $entity */
      $items = array_map(function ($entity) {
        return $entity->toLink();
      }, $entities);
    }

    $build['list'] = [
      '#theme' => 'item_list',
      '#items' => $items,
      '#empty' => $this->t('No issues.'),
    ];

    if (empty($items) && $config['add_on_empty']) {
      if ($task) {
        $build['list']['#empty'] = Link::createFromRoute($this->t('Add issue'), 'issue.task_issue.add_page', [
          'task' => $task->id(),
        ]);
      }
      /*elseif ($workplan) {
        $build['list']['#empty'] = Link::createFromRoute($this->t('Add issue'), 'issue.workplan_issue.add_page', [
          'workplan' => $workplan->id(),
        ]);
      }*/
      elseif (isset($project) && $project) {
        $build['list']['#empty'] = Link::createFromRoute($this->t('Add issue'), 'issue.project_issue.add_page', [
          'project' => $project->id(),
        ]);
      }
      elseif (isset($organization) && $organization) {
        $build['list']['#empty'] = Link::createFromRoute($this->t('Add issue'), 'issue.organization_issue.add_page', [
          'organization' => $organization->id(),
        ]);
      }
      else {
        $build['list']['#empty'] = Link::createFromRoute($this->t('Add issue'), 'entity.issue.add_page');
      }
    }

    if ($config['more_link']) {
      if ($task) {
        $url = Url::fromRoute('issue.task_issue', [
          'task' => $task->id(),
        ]);
      }
      elseif ($workplan) {
        $url = Url::fromRoute('issue.workplan_issue', [
          'workplan' => $workplan->id(),
        ]);
      }
      elseif ($project) {
        $url = Url::fromRoute('issue.project_issue', [
          'project' => $project->id(),
        ]);
      }
      elseif ($organization)
        $url = Url::fromRoute('issue.organization_issue', [
          'organization' => $organization->id(),
        ]);
      else {
        $url = Url::fromRoute('entity.issue.collection');
      }
      $build['more_link'] = [
        '#type' => 'more_link',
        '#url' => $url,
      ];
    }

    return $build;
  }

}
