<?php

namespace Drupal\issue;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the issue entity type.
 *
 * @see \Drupal\issue\Entity\Issue
 */
class IssueAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    switch ($operation) {
      case 'update':
        if (in_array($entity->status->value, [
          'submitted',
          'approved',
          'rejected',
          'closed',
          'canceled',
        ])) {
          return AccessResult::forbidden();
        }
        break;

      case 'delete':
        if ($entity->status->value != 'draft') {
          return AccessResult::forbidden();
        }
        break;
    }

    return parent::checkAccess($entity, $operation, $account);
  }

}
