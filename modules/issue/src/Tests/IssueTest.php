<?php

namespace Drupal\issue\Tests;

/**
 * Tests issue entity.
 *
 * @group issue
 */
class IssueTest extends IssueTestBase {

  /**
   * A user with project admin permission.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * Modules to install.
   *
   * @var array
   */
  public static $modules = ['block', 'views'];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->adminUser = $this->drupalCreateUser([
      'administer projects',
      'administer tasks',
      'access task',
    ]);
  }

  /**
   * Tests the project issue page.
   */
  public function testProjectIssue() {
    $this->drupalPlaceBlock('local_tasks_block');
    $this->drupalPlaceBlock('local_actions_block');

    $this->drupalLogin($this->adminUser);

    $this->drupalGet('admin/project/' . $this->project->id());
    $this->assertLinkByHref('admin/project/' . $this->project->id() . '/issue');

    $this->clickLink(t('Issues'));
    $this->assertResponse(200);
    $this->assertLinkByHref('admin/project/' . $this->project->id() . '/issue/add');

    $this->clickLink(t('Add issue'));
    $this->assertResponse(200);
  }

  /**
   * Tests the workplan issue page.
   */
  public function testWorkplanIssue() {
    $this->drupalPlaceBlock('local_tasks_block');

    $this->drupalLogin($this->adminUser);

    $this->drupalGet('admin/workplan/' . $this->workplan->id());
    $this->assertLinkByHref('admin/workplan/' . $this->workplan->id() . '/issue');

    $this->clickLink(t('Issues'));
    $this->assertResponse(200);
  }

  /**
   * Tests the task issue page.
   */
  public function testTaskIssue() {
    $this->drupalPlaceBlock('local_tasks_block');
    $this->drupalPlaceBlock('local_actions_block');

    $this->drupalLogin($this->adminUser);

    $this->drupalGet('admin/task/' . $this->task->id());
    $this->assertLinkByHref('admin/task/' . $this->task->id() . '/issue');

    $this->clickLink(t('Issues'));
    $this->assertResponse(200);
    $this->assertLinkByHref('admin/task/' . $this->task->id() . '/issue/add');

    $this->clickLink(t('Add issue'));
    $this->assertResponse(200);
  }

}
