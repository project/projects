<?php

namespace Drupal\issue\Tests;

/**
 * Tests issue type entities.
 *
 * @group issue
 */
class IssueTypeTest extends IssueTestBase {

  /**
   * A user with issue admin permission.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * Modules to install.
   *
   * @var array
   */
  public static $modules = ['block'];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->adminUser = $this->drupalCreateUser(['administer issue types']);
  }

  /**
   * Tests the list page.
   */
  public function testList() {
    $this->drupalPlaceBlock('local_actions_block');

    $this->drupalLogin($this->adminUser);

    $this->drupalGet('admin/issue/type');
    $this->assertResponse(200);
    $this->assertLinkByHref('admin/issue/type/add');

    $this->clickLink(t('Add issue type'));
    $this->assertResponse(200);

    $edit = [
      'label' => $this->randomMachineName(8),
      'id' => strtolower($this->randomMachineName(8)),
    ];
    $this->drupalPostForm(NULL, $edit, t('Save'));
    $this->assertResponse(200);
    $this->assertText($edit['label']);
  }

}
