<?php

namespace Drupal\issue\Tests;

use Drupal\issue\Entity\Issue;
use Drupal\issue\Entity\IssueType;
use Drupal\cbo_task\Tests\TaskTestBase;

/**
 * Sets up testing issue type and issue.
 */
abstract class IssueTestBase extends TaskTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['issue'];

  /**
   * A issue type.
   *
   * @var \Drupal\issue\IssueTypeInterface
   */
  protected $issueType;

  /**
   * A issue.
   *
   * @var \Drupal\issue\IssueInterface
   */
  protected $issue;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->issueType = $this->createIssueType();
    $this->issue = $this->createIssue($this->issueType->id());
  }

  /**
   * Creates a issue type based on default settings.
   */
  protected function createIssueType(array $settings = []) {
    // Populate defaults array.
    $settings += [
      'id' => strtolower($this->randomMachineName(8)),
      'label' => $this->randomMachineName(8),
    ];
    $entity = IssueType::create($settings);
    $entity->save();

    return $entity;
  }

  /**
   * Creates a issue based on default settings.
   */
  protected function createIssue($issue_type, array $settings = []) {
    // Populate defaults array.
    $settings += [
      'type' => $issue_type,
      'label' => $this->randomMachineName(8),
    ];
    $entity = Issue::create($settings);
    $entity->save();

    return $entity;
  }

}
