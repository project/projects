<?php

namespace Drupal\issue;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form handler for the issue edit forms.
 */
class IssueForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $issue = $this->entity;
    $insert = $issue->isNew();
    $issue->save();
    $issue_link = $issue->link($this->t('View'));
    $context = ['%title' => $issue->label(), 'link' => $issue_link];
    $t_args = ['%title' => $issue->link($issue->label())];

    if ($insert) {
      $this->logger('issue')->notice('Issue: added %title.', $context);
      drupal_set_message($this->t('Issue %title has been created.', $t_args));
    }
    else {
      $this->logger('issue')->notice('@type: updated %title.', $context);
      drupal_set_message($this->t('Issue %title has been updated.', $t_args));
    }
  }

}
