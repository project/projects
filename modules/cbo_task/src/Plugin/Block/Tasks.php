<?php

namespace Drupal\cbo_task\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\state_machine\WorkflowManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block to display Tasks.
 *
 * @Block(
 *   id = "tasks",
 *   admin_label = @Translation("Tasks")
 * )
 */
class Tasks extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The workflow manager service.
   *
   * @var \Drupal\state_machine\WorkflowManagerInterface
   */
  protected $workflowManager;

  /**
   * The currently active route match object.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a new SystemMenuBlock.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\state_machine\WorkflowManagerInterface $workflow_manager
   *   The workflow manager service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current active route match object.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, EntityTypeManagerInterface $entity_type_manager, WorkflowManagerInterface $workflow_manager, RouteMatchInterface $route_match) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->workflowManager = $workflow_manager;
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.workflow'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'task_status' => '',
      'progress_status' => '',
      'count' => 0,
      'add_on_empty' => FALSE,
      'more_link' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->configuration;

    /** @var \Drupal\state_machine\Plugin\Workflow\WorkflowInterface $workflow */
    $workflow = $this->workflowManager->createInstance('task_status');
    $states = $workflow->getStates();
    $options = array_map(function ($state) {
      return $state->getLabel();
    }, $states);
    $options = [
      '' => $this->t('-- Any --'),
    ] + $options;
    $form['task_status'] = [
      '#type' => 'select',
      '#title' => $this->t('Status'),
      '#default_value' => $config['task_status'],
      '#options' => $options,
    ];

    $form['progress_status'] = [
      '#type' => 'select',
      '#title' => $this->t('Progress status'),
      '#default_value' => $config['progress_status'],
      '#options' => [
        '' => $this->t('-- Any --'),
        'on_track' => $this->t('On Track'),
        'at_risk' => $this->t('At Risk'),
        'in_trouble' => $this->t('In Trouble'),
      ],
    ];

    $form['count'] = [
      '#type' => 'number',
      '#title' => $this->t('Item count'),
      '#min' => 1,
      '#default_value' => $config['count'] > 0 ? $config['count'] : '',
      '#description' => $this->t('The item count to display.'),
    ];

    $form['add_on_empty'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add on empty'),
      '#defalut_value' => $config['add_on_empty'],
    ];

    $form['more_link'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display more link'),
      '#default_value' => $config['more_link'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['task_status'] = $form_state->getValue('task_status');
    $this->configuration['progress_status'] = $form_state->getValue('progress_status');
    $this->configuration['count'] = $form_state->getValue('count');
    $this->configuration['more_link'] = $form_state->getValues('more_link');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->configuration;

    $storage = $this->entityTypeManager->getStorage('task');
    $query = $storage->getQuery();
    if (!empty($config['task_status'])) {
      $query->condition('status', $config['task_status']);
    }
    if (!empty($config['progress_status'])) {
      $query->condition('progress_status', $config['progress_status']);
    }

    if ($workplan = $this->routeMatch->getParameter('workplan')) {
      $query->condition('workplan', $workplan->id());
    }
    elseif ($project = $this->routeMatch->getParameter('project')) {
      $query->condition('project', $project->id());
    }

    if ($config['count'] > 0) {
      $query->range(NULL, $config['count']);
    }
    $query->sort('changed', 'DESC');

    $ids = $query->execute();

    $items = [];
    if (!empty($ids)) {
      $entities = $storage->loadMultiple($ids);
      /** @var \Drupal\issue\IssueInterface $entity */
      $items = array_map(function ($entity) {
        return $entity->toLink();
      }, $entities);
    }

    $build['list'] = [
      '#theme' => 'item_list',
      '#items' => $items,
      '#empty' => $this->t('No tasks.'),
    ];
    if (empty($items) && $config['add_on_empty']) {
      if ($workplan) {
        $build['list']['#empty'] = Link::createFromRoute($this->t('Add task'), 'task.workplan_task.add_page', [
          'workplan' => $workplan->id(),
        ]);
      }
      if (isset($project) && $project) {
        $build['list']['#empty'] = Link::createFromRoute($this->t('Add task'), 'task.project_task.add_page', [
          'project' => $project->id(),
        ]);
      }
      else {
        $build['list']['#empty'] = Link::createFromRoute($this->t('Add task'), 'entity.task.add_page');
      }
    }

    if ($config['more_link']) {
      if ($workplan) {
        $url = Url::fromRoute('task.workplan_task', [
          'workplan' => $workplan->id(),
        ]);
      }
      elseif (isset($project) && $project) {
        $url = Url::fromRoute('task.project_task', [
          'project' => $project->id(),
        ]);
      }
      else {
        $url = Url::fromRoute('entity.task.collection');
      }
      $build['more_link'] = [
        '#type' => 'more_link',
        '#url' => $url,
      ];
    }

    return $build;
  }

}
