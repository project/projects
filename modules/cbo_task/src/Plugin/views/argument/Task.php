<?php

namespace Drupal\cbo_task\Plugin\views\argument;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\cbo_task\TaskStorageInterface;
use Drupal\views\Plugin\views\argument\NumericArgument;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Argument handler for task tid.
 *
 * @ingroup views_argument_handlers
 *
 * @ViewsArgument("task")
 */
class Task extends NumericArgument implements ContainerFactoryPluginInterface {

  /**
   * Task storage handler.
   *
   * @var \Drupal\cbo_task\TaskStorageInterface
   */
  protected $taskStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, TaskStorageInterface $task_storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->taskStorage = $task_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity.manager')->getStorage('task')
    );
  }

  /**
   * Override the behavior of title(). Get the title of the task.
   */
  public function title() {
    // There might be no valid argument.
    if ($this->argument) {
      $task = $this->taskStorage->load($this->argument);
      if (!empty($task)) {
        return $task->label();
      }
    }
    return $this->t('No name');
  }

}
