<?php

namespace Drupal\cbo_task\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\cbo_task\TaskTypeInterface;

/**
 * Defines the Task type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "task_type",
 *   label = @Translation("Task type"),
 *   label_collection = @Translation("Task type"),
 *   handlers = {
 *     "access" = "Drupal\cbo_task\TaskTypeAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\cbo_task\TaskTypeForm",
 *       "delete" = "Drupal\cbo_task\Form\TaskTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\cbo_task\TaskTypeListBuilder",
 *   },
 *   admin_permission = "administer task types",
 *   config_prefix = "type",
 *   bundle_of = "task",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   links = {
 *     "add-form" = "/admin/task/type/add",
 *     "edit-form" = "/admin/task/type/{task_type}/edit",
 *     "delete-form" = "/admin/task/type/{task_type}/delete",
 *     "collection" = "/admin/task/type",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *   }
 * )
 */
class TaskType extends ConfigEntityBundleBase implements TaskTypeInterface {

  /**
   * The machine name of this task type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the task type.
   *
   * @var string
   */
  protected $name;

  /**
   * A brief description of this task type.
   *
   * @var string
   */
  protected $description;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

}
