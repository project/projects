<?php

namespace Drupal\cbo_task;

use Drupal\Core\Entity\ContentEntityStorageInterface;

/**
 * Defines an interface for task entity storage classes.
 */
interface TaskStorageInterface extends ContentEntityStorageInterface {

  /**
   * Finds all parents of a given task ID.
   *
   * @param int $tid
   *   Task ID to retrieve parents for.
   *
   * @return \Drupal\cbo_task\TaskInterface[]
   *   An array of task objects which are the parents of the task $tid.
   */
  public function loadParents($tid);

}
