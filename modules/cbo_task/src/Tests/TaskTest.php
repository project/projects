<?php

namespace Drupal\cbo_task\Tests;

/**
 * Tests task entities.
 *
 * @group cbo_task
 */
class TaskTest extends TaskTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  public static $modules = ['block', 'views'];

  /**
   * Tests the edit page.
   */
  public function testEdit() {
    $this->drupalPlaceBlock('local_tasks_block');

    $this->drupalLogin($this->projectManagerUser);

    $this->drupalGet('admin/task/' . $this->task->id());
    $this->assertLinkByHref('admin/task/' . $this->task->id() . '/edit');

    $this->clickLink(t('Edit'));
    $this->assertResponse(200);

    $edit = [
      'title[0][value]' => $this->randomMachineName(8),
    ];
    $this->drupalPostForm(NULL, $edit, t('Save'));
    $this->assertResponse(200);
    $this->assertText($edit['title[0][value]']);
  }

  /**
   * Tests the project tasks page.
   */
  public function testProjectTask() {
    $this->drupalPlaceBlock('local_tasks_block');
    $this->drupalPlaceBlock('local_actions_block');

    $this->drupalLogin($this->projectManagerUser);

    $this->drupalGet('admin/project/' . $this->project->id());
    $this->assertLinkByHref('admin/project/' . $this->project->id() . '/task');

    $this->clickLink(t('Tasks'), 1);
    $this->assertResponse(200);
    $this->assertLinkByHref('admin/project/' . $this->project->id() . '/task/add');

    $this->clickLink(t('Add task'));
    $this->assertResponse(200);
  }

  /**
   * Tests the workplan tasks page.
   */
  public function testWorkplanTask() {
    $this->drupalPlaceBlock('local_tasks_block');
    $this->drupalPlaceBlock('local_actions_block');

    $this->drupalLogin($this->projectManagerUser);

    $this->drupalGet('/admin/workplan/' . $this->workplan->id());
    $this->assertLinkByHref('/admin/workplan/' . $this->workplan->id() . '/task');

    $this->clickLink(t('Tasks'), 1);
    $this->assertResponse(200);
    $this->assertLinkByHref('admin/workplan/' . $this->workplan->id() . '/task/add');

    $this->clickLink(t('Add task'));
    $this->assertResponse(200);
  }

  /**
   * Tests the subtasks page.
   */
  public function testSubTasks() {
    $this->drupalPlaceBlock('local_tasks_block');
    $this->drupalPlaceBlock('local_actions_block');

    $this->drupalLogin($this->projectManagerUser);

    $this->drupalGet('admin/task/' . $this->task->id());
    $this->assertResponse(200);
    $this->assertLinkByHref('admin/task/' . $this->task->id() . '/task');

    $this->clickLink(t('Subtasks'));
    $this->assertResponse(200);
    $this->assertLinkByHref('admin/task/' . $this->task->id() . '/task/add');

    $this->clickLink(t('Add subtask'));
    $this->assertResponse(200);
  }

}
