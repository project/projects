<?php

namespace Drupal\cbo_task\Tests;

use Drupal\cbo_task\Entity\Task;
use Drupal\cbo_task\Entity\TaskType;
use Drupal\workplan\Tests\WorkplanTestBase;

/**
 * Setup modules.
 */
abstract class TaskTestBase extends WorkplanTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['cbo_task'];

  /**
   * A task type.
   *
   * @var \Drupal\cbo_task\TaskTypeInterface
   */
  protected $taskType;

  /**
   * A task.
   *
   * @var \Drupal\cbo_task\TaskInterface
   */
  protected $task;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->taskType = $this->createTaskType();
    $this->task = $this->createTask($this->taskType->id(), [
      'project' => $this->project->id(),
      'workplan' => $this->workplan->id(),
    ]);
  }

  /**
   * Creates a task type based on default settings.
   */
  protected function createTaskType(array $settings = []) {
    // Populate defaults array.
    $settings += [
      'id' => strtolower($this->randomMachineName(8)),
      'label' => $this->randomMachineName(8),
    ];
    $entity = TaskType::create($settings);
    $entity->save();

    return $entity;
  }

  /**
   * Creates a task based on default settings.
   */
  protected function createTask($task_type, array $settings = []) {
    // Populate defaults array.
    $settings += [
      'type' => $task_type,
      'label' => $this->randomMachineName(8),
    ];
    $entity = Task::create($settings);
    $entity->save();

    return $entity;
  }

}
