<?php

namespace Drupal\cbo_task;

use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides breadcrumb builder for task.
 */
class TaskBreadcrumbBuilder implements BreadcrumbBuilderInterface {
  use StringTranslationTrait;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs the TaskBreadcrumbBuilder.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    return $route_match->getRouteName() == 'entity.task.canonical'
      && $route_match->getParameter('task') instanceof TaskInterface;
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    $breadcrumb = new Breadcrumb();
    $breadcrumb->addLink(Link::createFromRoute($this->t('Home'), '<front>'));
    $task = $route_match->getParameter('task');
    $breadcrumb->addCacheableDependency($task);
    if ($organization = $task->getOrganization()) {
      $breadcrumb->addCacheableDependency($organization);
      $breadcrumb->addLink($organization->toLink());
    }
    if ($project = $task->getProject()) {
      $breadcrumb->addCacheableDependency($project);
      $breadcrumb->addLink($project->toLink());
    }
    if ($workplan = $task->getWorkplan()) {
      $breadcrumb->addCacheableDependency($workplan);
      $breadcrumb->addLink($workplan->toLink());
    }
    $parents = $this->entityTypeManager->getStorage('task')
      ->loadParents($task->id());
    // Remove current task being accessed.
    array_shift($parents);
    foreach (array_reverse($parents) as $task) {
      $breadcrumb->addCacheableDependency($task);
      $breadcrumb->addLink($task->toLink());
    }

    // This breadcrumb builder is based on a route parameter, and hence it
    // depends on the 'route' cache context.
    $breadcrumb->addCacheContexts(['route']);

    return $breadcrumb;
  }

}
