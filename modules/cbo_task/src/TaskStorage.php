<?php

namespace Drupal\cbo_task;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * Defines the storage handler class for tasks.
 */
class TaskStorage extends SqlContentEntityStorage implements TaskStorageInterface {

  /**
   * Array of loaded parents keyed by child task ID.
   *
   * @var array
   */
  protected $parents = [];

  /**
   * {@inheritdoc}
   */
  public function loadParents($tid) {
    if (!isset($this->parents[$tid])) {
      $parents = [];
      if ($entity = $this->load($tid)) {
        $parents[$entity->id()] = $entity;

        while ($entity = $entity->getParent()) {
          $parents[$entity->id()] = $entity;
        }
      }

      $this->parents[$tid] = $parents;
    }
    return $this->parents[$tid];
  }

}
