<?php

namespace Drupal\cbo_task;

use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * View builder handler for tasks.
 */
class TaskViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildComponents(array &$build, array $entities, array $displays, $view_mode) {
    /** @var \Drupal\cbo_task\TaskInterface[] $entities */
    if (empty($entities)) {
      return;
    }

    parent::buildComponents($build, $entities, $displays, $view_mode);

    foreach ($entities as $id => $entity) {
      $bundle = $entity->bundle();
      $display = $displays[$bundle];

      if ($display->getComponent('child_tasks')) {
        $items = [];
        if ($children = $this->entityManager->getStorage('task')->loadByProperties(['parent' => $id])) {
          $items = array_map(function ($child) {
            return $child->toLink();
          }, $children);
        }
        $build[$id]['child_tasks'] = [
          '#type' => 'details',
          '#title' => t('Child Tasks'),
          '#open' => TRUE,
          'list' => [
            '#theme' => 'item_list',
            '#items' => $items,
            '#empty' => Link::createFromRoute(
              'Add child task',
              'task.subtask.add_page',
              ['task' => $id]
            ),
          ],
          'more' => [
            '#type' => 'more_link',
            '#url' => Url::fromRoute('task.subtask', ['task' => $id]),
          ],
        ];
      }
    }
  }

}
