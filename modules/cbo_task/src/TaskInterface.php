<?php

namespace Drupal\cbo_task;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a task entity.
 */
interface TaskInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   * Get the organization.
   *
   * @return \Drupal\cbo_organization\OrganizationInterface|null
   *   The organization.
   */
  public function getOrganization();

  /**
   * Get the project.
   *
   * @return \Drupal\cbo_project\ProjectInterface|null
   *   The project.
   */
  public function getProject();

  /**
   * Get the workplan.
   *
   * @return \Drupal\workplan\WorkplanInterface|null
   *   The workplan.
   */
  public function getWorkplan();

  /**
   * Get the parent task.
   *
   * @return \Drupal\cbo_task\TaskInterface|null
   *   The parent task.
   */
  public function getParent();

}
