<?php

namespace Drupal\cbo_task;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form handler for the task edit forms.
 */
class TaskForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  protected function prepareEntity() {
    /** @var \Drupal\cbo_task\TaskInterface $task */
    $task = $this->entity;
    if ($task->isNew()) {
      if (!$task->workplan->target_id) {
        if ($parent = $task->parent->entity) {
          $task->workplan->target_id = $parent->workplan->target_id;
        }
      }
      if (!$task->project->target_id) {
        if ($workplan = $task->workplan->entity) {
          $task->project->target_id = $workplan->project->target_id;
        }
        elseif ($parent = $task->parent->entity) {
          $task->proejct->target_id = $parent->project->target_id;
        }
      }
      if (!$task->organization->target_id) {
        if ($project = $task->project->entity) {
          $task->organization->target_id = $project->organization->target_id;
        }
        elseif ($workplan = $task->workplan->entity) {
          $task->organization->target_id = $workplan->organization->target_id;
        }
        elseif ($parent = $task->parent->entity) {
          $task->organization->target_id = $parent->organization->target_id;
        }
      }

      if ($project = $task->project->entity) {
        $task->schedule_dates->value = $project->schedule_dates->value;
        $task->schedule_dates->end_value = $project->schedule_dates->end_value;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form['advanced'] = [
      '#type' => 'vertical_tabs',
      '#attributes' => ['class' => ['entity-meta']],
      '#weight' => 99,
    ];
    $form = parent::form($form, $form_state);

    $form['dates'] = [
      '#type' => 'details',
      '#group' => 'advanced',
      '#title' => $this->t('Dates'),
    ];

    if (isset($form['actual_dates'])) {
      $form['actual_dates']['#group'] = 'dates';
    }
    if (isset($form['schedule_dates'])) {
      $form['schedule_dates']['#group'] = 'dates';
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $task = $this->entity;
    $insert = $task->isNew();
    $task->save();
    $task_link = $task->link($this->t('View'));
    $context = ['%title' => $task->label(), 'link' => $task_link];
    $t_args = ['%title' => $task->link($task->label())];

    if ($insert) {
      $this->logger('task')->notice('Task: added %title.', $context);
      drupal_set_message($this->t('Task %title has been created.', $t_args));
    }
    else {
      $this->logger('task')->notice('Task: updated %title.', $context);
      drupal_set_message($this->t('Task %title has been updated.', $t_args));
    }
  }

}
