<?php

namespace Drupal\cbo_task\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\RendererInterface;
use Drupal\cbo_project\ProjectInterface;
use Drupal\cbo_task\TaskInterface;
use Drupal\cbo_task\TaskTypeInterface;
use Drupal\workplan\WorkplanInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Task routes.
 */
class TaskController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a NodeController object.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   */
  public function __construct(RendererInterface $renderer) {
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('renderer')
    );
  }

  /**
   * Provides add project task page.
   */
  public function projectTaskAddPage(ProjectInterface $project) {
    $bundles = $this->entityManager()->getBundleInfo('task');
    $build = [
      '#theme' => 'entity_add_list',
      '#bundles' => [],
    ];

    $link_text = $this->t('Add a new task type.');
    $build['#add_bundle_message'] = $this->t('There is no @entity_type yet. @add_link', [
      '@entity_type' => $this->t('task type'),
      '@add_link' => Link::createFromRoute($link_text, 'entity.task_type.add_form')->toString(),
    ]);
    // Filter out the bundles the user doesn't have access to.
    $access_control_handler = $this->entityTypeManager()->getAccessControlHandler('task');
    foreach ($bundles as $bundle_name => $bundle_info) {
      $access = $access_control_handler->createAccess($bundle_name, NULL, [], TRUE);
      if (!$access->isAllowed()) {
        unset($bundles[$bundle_name]);
      }
      $this->renderer->addCacheableDependency($build, $access);
    }

    // Redirect if there's only one bundle available.
    if (count($bundles) == 1) {
      $bundle_names = array_keys($bundles);
      return $this->redirect('task.project_task.add_form', [
        'project' => $project->id(),
        'task_type' => reset($bundle_names),
      ]);
    }
    // Prepare the #bundles array for the template.
    foreach ($bundles as $bundle_name => $bundle_info) {
      $build['#bundles'][$bundle_name] = [
        'label' => $bundle_info['label'],
        'description' => isset($bundle_info['description']) ? $bundle_info['description'] : '',
        'add_link' => Link::createFromRoute(
          $bundle_info['label'],
          'task.project_task.add_form',
          ['project' => $project->id(), 'task_type' => $bundle_name]
        ),
      ];
    }

    return $build;
  }

  /**
   * Provides add project task form.
   */
  public function projectTaskAddForm(ProjectInterface $project, TaskTypeInterface $task_type) {
    $task = $this->entityManager()->getStorage('task')->create([
      'organization' => $project->organization->target_id,
      'project' => $project->id(),
      'type' => $task_type->id(),
    ]);

    $form = $this->entityFormBuilder()->getForm($task);

    return $form;
  }

  /**
   * Provides add workplan task page.
   */
  public function workplanTaskAddPage(WorkplanInterface $workplan) {
    $bundles = $this->entityManager()->getBundleInfo('task');
    $build = [
      '#theme' => 'entity_add_list',
      '#bundles' => [],
    ];

    $link_text = $this->t('Add a new task type.');
    $build['#add_bundle_message'] = $this->t('There is no @entity_type yet. @add_link', [
      '@entity_type' => $this->t('task type'),
      '@add_link' => Link::createFromRoute($link_text, 'entity.task_type.add_form')->toString(),
    ]);
    // Filter out the bundles the user doesn't have access to.
    $access_control_handler = $this->entityTypeManager()->getAccessControlHandler('task');
    foreach ($bundles as $bundle_name => $bundle_info) {
      $access = $access_control_handler->createAccess($bundle_name, NULL, [], TRUE);
      if (!$access->isAllowed()) {
        unset($bundles[$bundle_name]);
      }
      $this->renderer->addCacheableDependency($build, $access);
    }

    // Redirect if there's only one bundle available.
    if (count($bundles) == 1) {
      $bundle_names = array_keys($bundles);
      return $this->redirect('task.workplan_task.add_form', [
        'workplan' => $workplan->id(),
        'task_type' => reset($bundle_names),
      ]);
    }
    // Prepare the #bundles array for the template.
    foreach ($bundles as $bundle_name => $bundle_info) {
      $build['#bundles'][$bundle_name] = [
        'label' => $bundle_info['label'],
        'description' => isset($bundle_info['description']) ? $bundle_info['description'] : '',
        'add_link' => Link::createFromRoute(
          $bundle_info['label'],
          'task.workplan_task.add_form',
          ['workplan' => $workplan->id(), 'task_type' => $bundle_name]
        ),
      ];
    }

    return $build;
  }

  /**
   * Provides add workplan task form.
   */
  public function workplanTaskAddForm(WorkplanInterface $workplan, TaskTypeInterface $task_type) {
    $task = $this->entityManager()->getStorage('task')->create([
      'organization' => $workplan->organization->target_id,
      'workplan' => $workplan->id(),
      'type' => $task_type->id(),
    ]);

    $form = $this->entityFormBuilder()->getForm($task);

    return $form;
  }

  /**
   * Provides add subtask page.
   */
  public function subtaskAddPage(TaskInterface $task) {
    $bundles = $this->entityManager()->getBundleInfo('task');
    $build = [
      '#theme' => 'entity_add_list',
      '#bundles' => [],
    ];

    $link_text = $this->t('Add a new task type.');
    $build['#add_bundle_message'] = $this->t('There is no @entity_type yet. @add_link', [
      '@entity_type' => $this->t('task type'),
      '@add_link' => Link::createFromRoute($link_text, 'entity.task_type.add_form')->toString(),
    ]);
    // Filter out the bundles the user doesn't have access to.
    $access_control_handler = $this->entityTypeManager()->getAccessControlHandler('task');
    foreach ($bundles as $bundle_name => $bundle_info) {
      $access = $access_control_handler->createAccess($bundle_name, NULL, [], TRUE);
      if (!$access->isAllowed()) {
        unset($bundles[$bundle_name]);
      }
      $this->renderer->addCacheableDependency($build, $access);
    }

    // Redirect if there's only one bundle available.
    if (count($bundles) == 1) {
      $bundle_names = array_keys($bundles);
      return $this->redirect('task.subtask.add_form', [
        'task' => $task->id(),
        'task_type' => reset($bundle_names),
      ]);
    }
    // Prepare the #bundles array for the template.
    foreach ($bundles as $bundle_name => $bundle_info) {
      $build['#bundles'][$bundle_name] = [
        'label' => $bundle_info['label'],
        'description' => isset($bundle_info['description']) ? $bundle_info['description'] : '',
        'add_link' => Link::createFromRoute(
          $bundle_info['label'],
          'task.subtask.add_form',
          ['task' => $task->id(), 'task_type' => $bundle_name]
        ),
      ];
    }

    return $build;
  }

  /**
   * Provides add subtask form.
   */
  public function subtaskAddForm(TaskInterface $task, TaskTypeInterface $task_type) {
    $child = $this->entityManager()->getStorage('task')->create([
      'organization' => $task->organization->target_id,
      'project' => $task->project->target_id,
      'parent' => $task->id(),
      'type' => $task_type->id(),
    ]);

    $form = $this->entityFormBuilder()->getForm($child);

    return $form;
  }

  /**
   * The _title_callback for the task.add route.
   *
   * @param \Drupal\cbo_task\TaskTypeInterface $task_type
   *   The current task type.
   *
   * @return string
   *   The page title
   */
  public function addPageTitle(TaskTypeInterface $task_type) {
    return $this->t('Create @name', ['@name' => $task_type->label()]);
  }

}
