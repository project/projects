<?php

namespace Drupal\cbo_task;

use Drupal\Core\Entity\EntityListBuilder;

/**
 * Defines a class to build a listing of task entities.
 *
 * @see \Drupal\cbo_task\Entity\Task
 */
class TaskListBuilder extends EntityListBuilder {

}
