<?php

namespace Drupal\cbo_task;

use Drupal\views\EntityViewsData;

/**
 * Provides the views data for the task entity types.
 */
class TaskViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['task']['bulk_form'] = [
      'title' => $this->t('Operations bulk form'),
      'help' => $this->t('Add a form element that lets you run operations on multiple tasks.'),
      'field' => [
        'id' => 'bulk_form',
      ],
    ];

    $data['task']['project']['argument']['id'] = 'project';
    $data['task']['workplan']['argument']['id'] = 'workplan';
    $data['task']['parent']['argument']['id'] = 'task';
    $data['task']['status']['filter']['id'] = 'state_machine_state';
    // $data['task']['progress_status']['filter']['id'] = 'list_field';

    return $data;
  }

}
