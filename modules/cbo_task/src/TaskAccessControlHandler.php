<?php

namespace Drupal\cbo_task;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the task entity type.
 *
 * @see \Drupal\cbo_task\Entity\Task
 */
class TaskAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $task, $operation, AccountInterface $account) {
    /** @var \Drupal\cbo_task\TaskInterface $task */
    if ($workplan = $task->getWorkplan()) {
      switch ($operation) {
        case 'view':
        case 'update':
          return $workplan->access($operation, $account, TRUE);

        case 'delete':
          return $workplan->access('update', $account, TRUE);
      }
    }
    elseif ($project = $task->getProject()) {
      switch ($operation) {
        case 'view':
        case 'update':
          return $project->access($operation, $account, TRUE);

        case 'delete':
          return $project->access('update', $account, TRUE);
      }
    }

    return parent::checkAccess($task, $operation, $account);
  }

}
