<?php

namespace Drupal\cbo_task\Form;

use Drupal\Core\Entity\EntityDeleteForm;
use Drupal\Core\Form\FormStateInterface;

class TaskTypeDeleteForm extends EntityDeleteForm {

  public function buildForm(array $form, FormStateInterface $form_state) {
    $num_entities = $this->entityTypeManager->getStorage('task')->getQuery()
      ->condition('type', $this->entity->id())
      ->count()
      ->execute();
    if ($num_entities) {
      $caption = '<p>' . $this->formatPlural($num_entities, '%type is used by 1 task on your system. You can not remove this task type until you have removed all of the %type task.', '%type is used by @count pieces of task on your system. You may not remove %type until you have removed all the %type task.', ['%type' => $this->entity->label()]) . '</p>';
      $form['#title'] = $this->getQuestion();
      $form['description'] = ['#markup' => $caption];
      return $form;
    }

    return parent::buildForm($form, $form_state);
  }

}
