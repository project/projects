<?php

namespace Drupal\projects_demo\Tests;

/**
 * Tests projects_demo module.
 *
 * @group projects_demo
 */
class ProjectsDemoTest extends ProjectsDemoTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  public static $modules = ['block', 'views'];

  /**
   * Tests the data import.
   */
  public function testDataImport() {
    $this->drupalLogin($this->projectManagerUser);

    $this->drupalGet('admin/organization');
    $this->assertLink('Vision Corp Operations', 0, 'Organization imported');

    $this->drupalGet('admin/resource/people');
    $this->assertText('Marlin', 'People imported');
  }

}
