<?php

namespace Drupal\projects_demo\Tests;

use Drupal\projects\Tests\ProjectsTestBase;

/**
 * Sets up modules.
 */
abstract class ProjectsDemoTestBase extends ProjectsTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['projects_demo'];

}
