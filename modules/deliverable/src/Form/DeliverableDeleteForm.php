<?php

namespace Drupal\deliverable\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting a deliverable.
 */
class DeliverableDeleteForm extends ContentEntityDeleteForm {

}
