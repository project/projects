<?php

namespace Drupal\deliverable\Form;

use Drupal\Core\Entity\EntityDeleteForm;

/**
 * Provides a form for deliverable type deletion.
 */
class DeliverableTypeDeleteConfirm extends EntityDeleteForm {

}
