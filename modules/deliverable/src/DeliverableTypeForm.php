<?php

namespace Drupal\deliverable;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form handler for deliverable type forms.
 */
class DeliverableTypeForm extends BundleEntityFormBase {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $type = $this->entity;
    if ($type->isNew()) {
      $form['#title'] = $this->t('Add deliverable type');
    }
    else {
      $form['#title'] = $this->t('Edit deliverable type');
    }

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#default_value' => $type->label(),
      '#maxlength' => 255,
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $type->id(),
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#machine_name' => [
        'exists' => ['Drupal\deliverable\Entity\DeliverableType', 'load'],
        'source' => ['label'],
      ],
    ];
    $form['class'] = [
      '#type' => 'select',
      '#title' => $this->t('Deliverable type class'),
      '#options' => [
        'document' => t('Document'),
        'other' => t('Other'),
      ],
      '#default_value' => $type->getClass(),
      '#required' => TRUE,
    ];
    $form['description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Description'),
      '#default_value' => $type->getDescription(),
    ];

    return $this->protectBundleIdElement($form);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var DeliverableTypeInterface $type */
    $type = $this->entity;
    $type->set('type', trim($type->id()));
    $type->set('name', trim($type->label()));

    $status = $type->save();

    $t_args = ['%name' => $type->label()];

    if ($status == SAVED_UPDATED) {
      drupal_set_message(t('The deliverable type %name has been updated.', $t_args));
    }
    elseif ($status == SAVED_NEW) {
      if ($type->getClass() == 'document') {
        deliverable_add_document_field($type);
      }
      drupal_set_message(t('The deliverable type %name has been added.', $t_args));
      $context = array_merge($t_args, ['link' => $type->link($this->t('View'), 'collection')]);
      $this->logger('deliverable')->notice('Added deliverable type %name.', $context);
    }
  }

}
