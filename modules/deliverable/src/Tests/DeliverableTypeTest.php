<?php

namespace Drupal\deliverable\Tests;

/**
 * Tests deliverable type entities.
 *
 * @group deliverable
 */
class DeliverableTypeTest extends DeliverableTestBase {

  /**
   * A user with deliverable admin permission.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * Modules to install.
   *
   * @var array
   */
  public static $modules = ['block'];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->adminUser = $this->drupalCreateUser(['administer deliverable types']);
  }

  /**
   * Tests the list page.
   */
  public function testList() {
    $this->drupalPlaceBlock('local_actions_block');

    $this->drupalLogin($this->adminUser);

    $this->drupalGet('admin/deliverable/type');
    $this->assertResponse(200);
    $this->assertLinkByHref('admin/deliverable/type/add');

    $this->clickLink(t('Add deliverable type'));
    $this->assertResponse(200);

    $edit = [
      'label' => $this->randomMachineName(8),
      'id' => strtolower($this->randomMachineName(8)),
      'class' => 'document',
    ];
    $this->drupalPostForm(NULL, $edit, t('Save'));
    $this->assertResponse(200);
    $this->assertText($edit['label']);
  }

}
