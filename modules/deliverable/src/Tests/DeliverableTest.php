<?php

namespace Drupal\deliverable\Tests;

/**
 * Tests deliverable entity.
 *
 * @group deliverable
 */
class DeliverableTest extends DeliverableTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  public static $modules = ['block', 'views'];

  /**
   * Tests the project deliverable page.
   */
  public function testProjectDeliverable() {
    $this->drupalPlaceBlock('local_tasks_block');
    $this->drupalPlaceBlock('local_actions_block');

    $this->drupalLogin($this->projectManagerUser);

    $this->drupalGet('admin/project/' . $this->project->id());
    $this->assertLinkByHref('admin/project/' . $this->project->id() . '/deliverable');

    $this->clickLink(t('Deliverables'), 1);
    $this->assertResponse(200);
    $this->assertLinkByHref('admin/project/' . $this->project->id() . '/deliverable/add');

    $this->clickLink(t('Add deliverable'));
    $this->assertResponse(200);
    $this->assertLinkByHref('admin/project/' . $this->project->id() . '/deliverable/add/document');

    $this->clickLink(t('Document'));
    $this->assertResponse(200);
  }

  /**
   * Tests the workplan deliverable page.
   */
  public function testWorkplanDeliverable() {
    $this->drupalPlaceBlock('local_tasks_block');

    $this->drupalLogin($this->projectManagerUser);

    $this->drupalGet('admin/workplan/' . $this->workplan->id());
    $this->assertLinkByHref('admin/workplan/' . $this->workplan->id() . '/deliverable');

    $this->clickLink(t('Deliverables'));
    $this->assertResponse(200);
  }

  /**
   * Tests the task deliverable page.
   */
  public function testTaskDeliverable() {
    $this->drupalPlaceBlock('local_tasks_block');
    $this->drupalPlaceBlock('local_actions_block');

    $this->drupalLogin($this->projectManagerUser);

    $this->drupalGet('admin/task/' . $this->task->id());
    $this->assertLinkByHref('admin/task/' . $this->task->id() . '/deliverable');

    $this->clickLink(t('Deliverables'), 1);
    $this->assertResponse(200);
    $this->assertLinkByHref('admin/task/' . $this->task->id() . '/deliverable/add');

    $this->clickLink(t('Add deliverable'));
    $this->assertResponse(200);
  }

}
