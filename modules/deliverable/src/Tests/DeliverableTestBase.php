<?php

namespace Drupal\deliverable\Tests;

use Drupal\deliverable\Entity\Deliverable;
use Drupal\deliverable\Entity\DeliverableType;
use Drupal\cbo_task\Tests\TaskTestBase;

/**
 * Sets up testing deliverable type and deliverable.
 */
abstract class DeliverableTestBase extends TaskTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['deliverable'];

  /**
   * A deliverable type.
   *
   * @var \Drupal\deliverable\DeliverableTypeInterface
   */
  protected $deliverableType;

  /**
   * A deliverable.
   *
   * @var \Drupal\deliverable\DeliverableInterface
   */
  protected $deliverable;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->deliverableType = $this->createDeliverableType();
    $this->deliverable = $this->createDeliverable($this->deliverableType->id());
  }

  /**
   * Creates a deliverable type based on default settings.
   */
  protected function createDeliverableType(array $settings = []) {
    // Populate defaults array.
    $settings += [
      'id' => strtolower($this->randomMachineName(8)),
      'label' => $this->randomMachineName(8),
      'class' => 'item',
    ];
    $entity = DeliverableType::create($settings);
    $entity->save();

    return $entity;
  }

  /**
   * Creates a deliverable based on default settings.
   */
  protected function createDeliverable($deliverable_type, array $settings = []) {
    // Populate defaults array.
    $settings += [
      'type' => $deliverable_type,
      'label' => $this->randomMachineName(8),
    ];
    $entity = Deliverable::create($settings);
    $entity->save();

    return $entity;
  }

}
