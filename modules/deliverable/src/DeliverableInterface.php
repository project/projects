<?php

namespace Drupal\deliverable;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a deliverable entity.
 */
interface DeliverableInterface extends ContentEntityInterface {

  /**
   * Get the organization.
   *
   * @return \Drupal\cbo_organization\OrganizationInterface|null
   *   The organization.
   */
  public function getOrganization();

  /**
   * Get the project.
   *
   * @return \Drupal\cbo_project\ProjectInterface|null
   *   The project.
   */
  public function getProject();

  /**
   * Get the workplan.
   *
   * @return \Drupal\workplan\WorkplanInterface|null
   *   The workplan.
   */
  public function getWorkplan();

  /**
   * Get the task.
   *
   * @return \Drupal\cbo_task\TaskInterface|null
   *   The task.
   */
  public function getTask();

}
