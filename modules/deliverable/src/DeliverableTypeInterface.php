<?php

namespace Drupal\deliverable;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * Provides an interface defining a deliverable type entity.
 */
interface DeliverableTypeInterface extends ConfigEntityInterface, EntityDescriptionInterface {

  /**
   * Gets the deliverable type class.
   *
   * @return string
   *   The deliverable type class.
   */
  public function getClass();

}
