<?php

namespace Drupal\deliverable;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form handler for the deliverable edit forms.
 */
class DeliverableForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $deliverable = $this->entity;
    $insert = $deliverable->isNew();
    $deliverable->save();
    $deliverable_link = $deliverable->link($this->t('View'));
    $context = ['%title' => $deliverable->label(), 'link' => $deliverable_link];
    $t_args = ['%title' => $deliverable->link($deliverable->label())];

    if ($insert) {
      $this->logger('deliverable')->notice('Deliverable: added %title.', $context);
      drupal_set_message($this->t('Deliverable %title has been created.', $t_args));
    }
    else {
      $this->logger('deliverable')->notice('@type: updated %title.', $context);
      drupal_set_message($this->t('Deliverable %title has been updated.', $t_args));
    }
  }

}
