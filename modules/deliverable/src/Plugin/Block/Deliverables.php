<?php

namespace Drupal\deliverable\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\state_machine\WorkflowManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block to display Deliverables.
 *
 * @Block(
 *   id = "deliverables",
 *   admin_label = @Translation("Deliverables")
 * )
 */
class Deliverables extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The workflow manager service.
   *
   * @var \Drupal\state_machine\WorkflowManagerInterface
   */
  protected $workflowManager;

  /**
   * The currently active route match object.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a new SystemMenuBlock.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\state_machine\WorkflowManagerInterface $workflow_manager
   *   The workflow manager service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current active route match object.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, EntityTypeManagerInterface $entity_type_manager, WorkflowManagerInterface $workflow_manager, RouteMatchInterface $route_match) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->workflowManager = $workflow_manager;
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.workflow'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'deliverable_status' => '',
      'progress_status' => '',
      'deliverable_due' => FALSE,
      'count' => 0,
      'add_on_empty' => FALSE,
      'more_link' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->configuration;

    /** @var \Drupal\state_machine\Plugin\Workflow\WorkflowInterface $workflow */
    $workflow = $this->workflowManager->createInstance('deliverable_status');
    $states = $workflow->getStates();
    $options = array_map(function ($state) {
      return $state->getLabel();
    }, $states);
    $options = [
      '' => $this->t('-- Any --'),
      'not_completed' => $this->t('-- Not Completed --'),
    ] + $options;
    $form['deliverable_status'] = [
      '#type' => 'select',
      '#title' => $this->t('Status'),
      '#default_value' => $config['deliverable_status'],
      '#options' => $options,
    ];

    $form['progress_status'] = [
      '#type' => 'select',
      '#title' => $this->t('Progress status'),
      '#default_value' => $config['progress_status'],
      '#options' => [
        '' => $this->t('-- Any --'),
        'on_track' => $this->t('On Track'),
        'at_risk' => $this->t('At Risk'),
        'in_trouble' => $this->t('In Trouble'),
      ],
    ];

    $form['deliverable_due'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Deliverable due'),
      '#default_value' => $config['deliverable_due'],
    ];

    $form['count'] = [
      '#type' => 'number',
      '#title' => $this->t('Item count'),
      '#min' => 1,
      '#default_value' => $config['count'] > 0 ? $config['count'] : '',
      '#description' => $this->t('The item count to display.'),
    ];

    $form['add_on_empty'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add on empty'),
      '#defalut_value' => $config['add_on_empty'],
    ];

    $form['more_link'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display more link'),
      '#default_value' => $config['more_link'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['deliverable_status'] = $form_state->getValue('deliverable_status');
    $this->configuration['progress_status'] = $form_state->getValue('progress_status');
    $this->configuration['count'] = $form_state->getValue('count');
    $this->configuration['more_link'] = $form_state->getValues('more_link');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->configuration;

    $storage = $this->entityTypeManager->getStorage('deliverable');
    $query = $storage->getQuery();
    if (!empty($config['deliverable_status'])) {
      if ($config['deliverable_status'] == 'not_completed') {
        $query->condition('status', ['completed', 'canceled'], 'NOT IN');
      }
      else {
        $query->condition('status', $config['deliverable_status']);
      }
    }
    if (!empty($config['progress_status'])) {
      $query->condition('progress_status', $config['progress_status']);
    }
    if ($config['deliverable_due']) {
      $query->condition('due_date', date('Y-m-d', REQUEST_TIME), '>=');
    }

    if ($task = $this->routeMatch->getParameter('task')) {
      $query->condition('task', $task->id());
    }
    elseif ($workplan = $this->routeMatch->getParameter('workplan')) {
      $tasks = $this->entityTypeManager->getStorage('task')->getQuery()
        ->condition('workplan', $workplan->id())
        ->execute();
      if (!empty($tasks)) {
        $query->condition('task', $tasks, 'IN');
      }
    }
    elseif ($project = $this->routeMatch->getParameter('project')) {
      $query->condition('project', $project->id());
    }

    if ($config['count'] > 0) {
      $query->range(NULL, $config['count']);
    }

    if ($config['deliverable_due']) {
      $query->sort('due_date', 'DESC');
    }
    else {
      $query->sort('changed', 'DESC');
    }

    $ids = $query->execute();

    $items = [];
    if (!empty($ids)) {
      $entities = $storage->loadMultiple($ids);
      $items = array_map(function ($entity) {
        return [
          '#markup' => $entity->due_date->value . ' ' . $entity->toLink()->toString(),
        ];
      }, $entities);
    }

    $build['list'] = [
      '#theme' => 'item_list',
      '#items' => $items,
      '#empty' => $this->t('No deliverables.'),
    ];
    if (empty($items) && $config['add_on_empty']) {
      if ($task) {
        $build['list']['#empty'] = Link::createFromRoute($this->t('Add deliverable'), 'deliverable.task_deliverable.add_page', [
          'task' => $task->id(),
        ]);
      }
      /*elseif ($workplan) {
        $build['list']['#empty'] = Link::createFromRoute($this->t('Add deliverable'), 'deliverable.workplan_deliverable.add_page', [
          'workplan' => $workplan->id(),
        ]);
      }*/
      elseif (isset($project) && $project) {
        $build['list']['#empty'] = Link::createFromRoute($this->t('Add deliverable'), 'deliverable.project_deliverable.add_page', [
          'project' => $project->id(),
        ]);
      }
      else {
        $build['list']['#empty'] = Link::createFromRoute($this->t('Add deliverable'), 'entity.deliverable.add_page');
      }
    }

    if ($config['more_link']) {
      if ($task) {
        $url = Url::fromRoute('deliverable.task_deliverable', [
          'task' => $task->id(),
        ]);
      }
      elseif (isset($workplan) && $workplan) {
        $url = Url::fromRoute('deliverable.workplan_deliverable', [
          'workplan' => $workplan->id(),
        ]);
      }
      elseif (isset($project) && $project) {
        $url = Url::fromRoute('deliverable.project_deliverable', [
          'project' => $project->id(),
        ]);
      }
      else {
        $url = Url::fromRoute('entity.deliverable.collection');
      }
      $build['more_link'] = [
        '#type' => 'more_link',
        '#url' => $url,
      ];
    }

    return $build;
  }

}
