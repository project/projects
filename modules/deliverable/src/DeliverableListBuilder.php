<?php

namespace Drupal\deliverable;

use Drupal\Core\Entity\EntityListBuilder;

/**
 * Defines a class to build a listing of deliverable entities.
 *
 * @see \Drupal\deliverable\Entity\Deliverable
 */
class DeliverableListBuilder extends EntityListBuilder {

}
