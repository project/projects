<?php

namespace Drupal\deliverable;

use Drupal\Core\Entity\EntityAccessControlHandler;

/**
 * Defines the access control handler for the deliverable type entity type.
 *
 * @see \Drupal\deliverable\Entity\DeliverableType
 */
class DeliverableTypeAccessControlHandler extends EntityAccessControlHandler {

}
