<?php

namespace Drupal\deliverable\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\RendererInterface;
use Drupal\deliverable\DeliverableTypeInterface;
use Drupal\cbo_project\ProjectInterface;
use Drupal\cbo_task\TaskInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Deliverable routes.
 */
class DeliverableController extends ControllerBase implements ContainerInjectionInterface {
  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a NodeController object.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   */
  public function __construct(RendererInterface $renderer) {
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('renderer')
    );
  }

  /**
   * Provides add project deliverable page.
   */
  public function projectDeliverableAddPage(ProjectInterface $project) {
    $bundles = $this->entityManager()->getBundleInfo('deliverable');
    $build = [
      '#theme' => 'entity_add_list',
      '#bundles' => [],
    ];

    $link_text = $this->t('Add a new deliverable type.');
    $build['#add_bundle_message'] = $this->t('There is no @entity_type yet. @add_link', [
      '@entity_type' => $this->t('deliverable type'),
      '@add_link' => Link::createFromRoute($link_text, 'entity.deliverable_type.add_form')->toString(),
    ]);
    // Filter out the bundles the user doesn't have access to.
    $access_control_handler = $this->entityTypeManager()->getAccessControlHandler('deliverable');
    foreach ($bundles as $bundle_name => $bundle_info) {
      $access = $access_control_handler->createAccess($bundle_name, NULL, [], TRUE);
      if (!$access->isAllowed()) {
        unset($bundles[$bundle_name]);
      }
      $this->renderer->addCacheableDependency($build, $access);
    }

    // Redirect if there's only one bundle available.
    if (count($bundles) == 1) {
      $bundle_names = array_keys($bundles);
      return $this->redirect('deliverable.project_deliverable.add_form', [
        'project' => $project->id(),
        'deliverable_type' => reset($bundle_names),
      ]);
    }
    // Prepare the #bundles array for the template.
    foreach ($bundles as $bundle_name => $bundle_info) {
      $build['#bundles'][$bundle_name] = [
        'label' => $bundle_info['label'],
        'description' => isset($bundle_info['description']) ? $bundle_info['description'] : '',
        'add_link' => Link::createFromRoute(
          $bundle_info['label'],
          'deliverable.project_deliverable.add_form',
          ['project' => $project->id(), 'deliverable_type' => $bundle_name]
        ),
      ];
    }

    return $build;
  }

  /**
   * Provides add project deliverable form.
   */
  public function projectDeliverableAddForm(ProjectInterface $project, DeliverableTypeInterface $deliverable_type) {
    $deliverable = $this->entityManager()->getStorage('deliverable')->create([
      'project' => $project->id(),
      'type' => $deliverable_type->id(),
    ]);

    $form = $this->entityFormBuilder()->getForm($deliverable);

    return $form;
  }

  /**
   * Provides add task deliverable page.
   */
  public function taskDeliverableAddPage(TaskInterface $task) {
    $bundles = $this->entityManager()->getBundleInfo('deliverable');
    $build = [
      '#theme' => 'entity_add_list',
      '#bundles' => [],
    ];

    $link_text = $this->t('Add a new deliverable type.');
    $build['#add_bundle_message'] = $this->t('There is no @entity_type yet. @add_link', [
      '@entity_type' => $this->t('deliverable type'),
      '@add_link' => Link::createFromRoute($link_text, 'entity.deliverable_type.add_form')->toString(),
    ]);
    // Filter out the bundles the user doesn't have access to.
    $access_control_handler = $this->entityTypeManager()->getAccessControlHandler('deliverable');
    foreach ($bundles as $bundle_name => $bundle_info) {
      $access = $access_control_handler->createAccess($bundle_name, NULL, [], TRUE);
      if (!$access->isAllowed()) {
        unset($bundles[$bundle_name]);
      }
      $this->renderer->addCacheableDependency($build, $access);
    }

    // Redirect if there's only one bundle available.
    if (count($bundles) == 1) {
      $bundle_names = array_keys($bundles);
      return $this->redirect('deliverable.task_deliverable.add_form', [
        'task' => $task->id(),
        'deliverable_type' => reset($bundle_names),
      ]);
    }
    // Prepare the #bundles array for the template.
    foreach ($bundles as $bundle_name => $bundle_info) {
      $build['#bundles'][$bundle_name] = [
        'label' => $bundle_info['label'],
        'description' => isset($bundle_info['description']) ? $bundle_info['description'] : '',
        'add_link' => Link::createFromRoute(
          $bundle_info['label'],
          'deliverable.task_deliverable.add_form',
          ['task' => $task->id(), 'deliverable_type' => $bundle_name]
        ),
      ];
    }

    return $build;
  }

  /**
   * Provides add task deliverable form.
   */
  public function taskDeliverableAddForm(TaskInterface $task, DeliverableTypeInterface $deliverable_type) {
    $values = [
      'task' => $task->id(),
      'type' => $deliverable_type->id(),
    ];
    if ($task->project->target_id) {
      $values['project'] = $task->project->target_id;
    }
    $deliverable = $this->entityManager()->getStorage('deliverable')
      ->create($values);

    $form = $this->entityFormBuilder()->getForm($deliverable);

    return $form;
  }

  /**
   * The _title_callback for the deliverable.add route.
   *
   * @param \Drupal\deliverable\DeliverableTypeInterface $deliverable_type
   *   The current deliverable type.
   *
   * @return string
   *   The page title
   */
  public function addPageTitle(DeliverableTypeInterface $deliverable_type) {
    return $this->t('Create @name', ['@name' => $deliverable_type->label()]);
  }

}
