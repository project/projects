<?php

namespace Drupal\deliverable;

use Drupal\Core\Entity\ContentEntityStorageInterface;

/**
 * Defines an interface for deliverable entity storage classes.
 */
interface DeliverableStorageInterface extends ContentEntityStorageInterface {

}
