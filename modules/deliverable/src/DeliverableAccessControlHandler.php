<?php

namespace Drupal\deliverable;

use Drupal\Core\Entity\EntityAccessControlHandler;

/**
 * Defines the access control handler for the deliverable entity type.
 *
 * @see \Drupal\deliverable\Entity\Deliverable
 */
class DeliverableAccessControlHandler extends EntityAccessControlHandler {

}
