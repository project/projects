<?php

namespace Drupal\deliverable;

use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides breadcrumb builder for deliverable.
 */
class DeliverableBreadcrumbBuilder implements BreadcrumbBuilderInterface {
  use StringTranslationTrait;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs the DeliverableBreadcrumbBuilder.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    return $route_match->getRouteName() == 'entity.deliverable.canonical'
      && $route_match->getParameter('deliverable') instanceof DeliverableInterface;
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    $breadcrumb = new Breadcrumb();
    $breadcrumb->addLink(Link::createFromRoute($this->t('Home'), '<front>'));
    $deliverable = $route_match->getParameter('deliverable');
    $breadcrumb->addCacheableDependency($deliverable);
    if ($organization = $deliverable->getOrganization()) {
      $breadcrumb->addCacheableDependency($organization);
      $breadcrumb->addLink($organization->toLink());
    }
    if ($project = $deliverable->getProject()) {
      $breadcrumb->addCacheableDependency($project);
      $breadcrumb->addLink($project->toLink());
    }
    if ($workplan = $deliverable->getWorkplan()) {
      $breadcrumb->addCacheableDependency($workplan);
      $breadcrumb->addLink($workplan->toLink());
    }
    if ($task = $deliverable->getTask()) {
      $parents = $this->entityTypeManager->getStorage('task')
        ->loadParents($task->id());
      foreach (array_reverse($parents) as $task) {
        $breadcrumb->addCacheableDependency($task);
        $breadcrumb->addLink($task->toLink());
      }
    }

    // This breadcrumb builder is based on a route parameter, and hence it
    // depends on the 'route' cache context.
    $breadcrumb->addCacheContexts(['route']);

    return $breadcrumb;
  }

}
