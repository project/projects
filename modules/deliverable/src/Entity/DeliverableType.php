<?php

namespace Drupal\deliverable\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\deliverable\DeliverableTypeInterface;

/**
 * Defines the Deliverable type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "deliverable_type",
 *   label = @Translation("Deliverable type"),
 *   handlers = {
 *     "access" = "Drupal\deliverable\DeliverableTypeAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\deliverable\DeliverableTypeForm",
 *       "delete" = "Drupal\deliverable\Form\DeliverableTypeDeleteConfirm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\deliverable\DeliverableTypeListBuilder",
 *   },
 *   admin_permission = "administer deliverable types",
 *   config_prefix = "type",
 *   bundle_of = "deliverable",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   links = {
 *     "add-form" = "/admin/deliverable/type/add",
 *     "edit-form" = "/admin/deliverable/type/{deliverable_type}/edit",
 *     "delete-form" = "/admin/deliverable/type/{deliverable_type}/delete",
 *     "collection" = "/admin/deliverable/type",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "class",
 *     "description",
 *   }
 * )
 */
class DeliverableType extends ConfigEntityBundleBase implements DeliverableTypeInterface {

  /**
   * The machine name of this deliverable type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the deliverable type.
   *
   * @var string
   */
  protected $label;

  /**
   * Deliverable type class: item, document, other.
   *
   * @var string
   */
  protected $class;

  /**
   * A brief description of this deliverable type.
   *
   * @var string
   */
  protected $description;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getClass() {
    return $this->class;
  }

}
