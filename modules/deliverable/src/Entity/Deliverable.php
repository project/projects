<?php

namespace Drupal\deliverable\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\deliverable\DeliverableInterface;

/**
 * Defines the deliverable entity class.
 *
 * @ContentEntityType(
 *   id = "deliverable",
 *   label = @Translation("Deliverable"),
 *   bundle_label = @Translation("Deliverable type"),
 *   handlers = {
 *     "storage" = "Drupal\deliverable\DeliverableStorage",
 *     "access" = "Drupal\deliverable\DeliverableAccessControlHandler",
 *     "views_data" = "Drupal\deliverable\DeliverableViewsData",
 *     "form" = {
 *       "default" = "Drupal\deliverable\DeliverableForm",
 *       "delete" = "Drupal\deliverable\Form\DeliverableDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\deliverable\DeliverableListBuilder",
 *   },
 *   base_table = "deliverable",
 *   entity_keys = {
 *     "id" = "did",
 *     "bundle" = "type",
 *     "label" = "title",
 *     "uuid" = "uuid",
 *   },
 *   bundle_entity_type = "deliverable_type",
 *   field_ui_base_route = "entity.deliverable_type.edit_form",
 *   admin_permission = "administer deliverables",
 *   links = {
 *     "add-page" = "/admin/deliverable/add",
 *     "add-form" = "/admin/deliverable/add/{deliverable_type}",
 *     "canonical" = "/admin/deliverable/{deliverable}",
 *     "edit-form" = "/admin/deliverable/{deliverable}/edit",
 *     "delete-form" = "/admin/deliverable/{deliverable}/delete",
 *     "collection" = "/admin/deliverable",
 *   },
 * )
 */
class Deliverable extends ContentEntityBase implements DeliverableInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function getOrganization() {
    return $this->getProject()->getOrganization();
  }

  /**
   * {@inheritdoc}
   */
  public function getProject() {
    return $this->get('project')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getWorkplan() {
    return $this->getTask()->getWorkplan();
  }

  /**
   * {@inheritdoc}
   */
  public function getTask() {
    return $this->get('task')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['number'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Number'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['project'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Project'))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'project')
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_label',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 0,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['task'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Task'))
      ->setSetting('target_type', 'task')
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_label',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 0,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['status'] = BaseFieldDefinition::create('state')
      ->setLabel(t('Status'))
      ->setRequired(TRUE)
      ->setDefaultValue('not_started')
      ->setSetting('workflow', 'deliverable_status')
      ->setDisplayOptions('view', [
        'type' => 'list_default',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['owner'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Owner'))
      ->setSetting('target_type', 'people')
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_label',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 0,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['due_date'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Due date'))
      ->setSetting('datetime_type', 'date')
      ->setDisplayOptions('view', [
        'type' => 'datetime_default',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_default',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['progress_status'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Progress status'))
      ->setDescription(t('Qualitative evaluation overall deliverable progress.'))
      ->setSetting('allowed_values', [
        'on_track' => t('On Track'),
        'at_risk' => t('At Risk'),
        'in_trouble' => t('In Trouble'),
      ])
      ->setDisplayOptions('view', [
        'type' => 'list_default',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['description'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Description'))
      ->setDisplayOptions('view', [
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'weight' => 0,
        'settings' => [
          'rows' => 4,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The timestamp that the deliverable was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the deliverable was last edited.'));

    return $fields;
  }

}
