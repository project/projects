<?php

namespace Drupal\deliverable;

use Drupal\views\EntityViewsData;

/**
 * Provides the views data for the deliverable entity type.
 */
class DeliverableViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['deliverable']['project']['argument']['id'] = 'project';
    $data['deliverable']['task']['argument']['id'] = 'task';
    $data['deliverable']['status']['filter']['id'] = 'state_machine_state';

    return $data;
  }

}
