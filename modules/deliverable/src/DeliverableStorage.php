<?php

namespace Drupal\deliverable;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * Defines the storage handler class for deliverable.
 */
class DeliverableStorage extends SqlContentEntityStorage implements DeliverableStorageInterface {

}
