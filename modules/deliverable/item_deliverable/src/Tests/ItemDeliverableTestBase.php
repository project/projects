<?php

namespace Drupal\item_deliverable\Tests;

use Drupal\deliverable\Tests\DeliverableTestBase;

/**
 * Sets up testing item_deliverable type and item_deliverable.
 */
abstract class ItemDeliverableTestBase extends DeliverableTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['item_deliverable'];

}
