<?php

namespace Drupal\item_deliverable\Tests;

/**
 * Tests item_deliverable entity.
 *
 * @group item_deliverable
 */
class ItemDeliverableTest extends ItemDeliverableTestBase {

  /**
   * A user with project admin permission.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * Modules to install.
   *
   * @var array
   */
  public static $modules = ['block', 'views'];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->adminUser = $this->drupalCreateUser([
      'administer deliverable types',
    ]);
  }

  /**
   * Tests the item_deliverable add page.
   */
  public function testItemDeliverableAdd() {
    $this->drupalLogin($this->adminUser);

    $this->drupalGet('admin/deliverable/type/add');

    $edit = [
      'label' => $this->randomMachineName(8),
      'id' => strtolower($this->randomMachineName(8)),
      'class' => 'item',
    ];
    $this->drupalPostForm(NULL, $edit, t('Save'));
    $this->assertResponse(200);
    $this->assertText($edit['label']);
  }

}
