<?php

/**
 * @file
 * The module that provide project and task deliverable.
 */

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\deliverable\DeliverableTypeInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\cbo_project\Entity\ProjectType;
use Drupal\cbo_task\Entity\TaskType;

/**
 * Implements hook_entity_extra_field_info().
 */
function deliverable_entity_extra_field_info() {
  $extra = [];

  foreach (ProjectType::loadMultiple() as $bundle) {
    $extra['project'][$bundle->id()]['display']['deliverable_due'] = [
      'label' => t('Deliverable Due'),
      'description' => 'Project deliverable due',
      'weight' => 0,
    ];
    $extra['project'][$bundle->id()]['display']['deliverables'] = [
      'label' => t('Deliverables'),
      'description' => 'Project deliverables',
      'weight' => 0,
    ];
  }

  $extra['workplan']['workplan']['display']['deliverables'] = [
    'label' => t('Deliverables'),
    'description' => 'Workplan deliverables',
    'weight' => 0,
  ];

  foreach (TaskType::loadMultiple() as $bundle) {
    $extra['task'][$bundle->id()]['display']['deliverables'] = [
      'label' => t('Deliverables'),
      'description' => 'Task deliverables',
      'weight' => 0,
    ];
  }

  return $extra;
}

/**
 * Implements hook_ENTITY_TYPE_view() for project entities.
 */
function deliverable_project_view(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {
  $block_manager = \Drupal::service('plugin.manager.block');

  if ($display->getComponent('deliverable_due')) {
    $block = $block_manager->createInstance('deliverables', [
      'deliverable_status' => 'not_completed',
      'deliverable_due' => TRUE,
    ]);
    $build['deliverable_due'] = [
      '#type' => 'details',
      '#title' => t('Deliverable Due'),
      '#open' => TRUE,
      'content' => $block->build(),
    ];
  }

  if ($display->getComponent('deliverables')) {
    $block = $block_manager->createInstance('deliverables', [
      'count' => 10,
      'add_on_empty' => TRUE,
      'more_link' => TRUE,
    ]);
    $build['deliverables'] = [
      '#type' => 'details',
      '#title' => t('Deliverables'),
      '#open' => TRUE,
      'content' => $block->build(),
    ];
  }
}

/**
 * Implements hook_ENTITY_TYPE_view() for workplan entities.
 */
function deliverable_workplan_view(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {
  $block_manager = \Drupal::service('plugin.manager.block');

  if ($display->getComponent('deliverables')) {
    $block = $block_manager->createInstance('deliverables', [
      'count' => 10,
      'add_on_empty' => TRUE,
      'more_link' => TRUE,
    ]);
    $build['deliverables'] = [
      '#type' => 'details',
      '#title' => t('Deliverables'),
      '#open' => TRUE,
      'content' => $block->build(),
    ];
  }
}

/**
 * Implements hook_ENTITY_TYPE_view() for task entities.
 */
function deliverable_task_view(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {
  $block_manager = \Drupal::service('plugin.manager.block');

  if ($display->getComponent('deliverables')) {
    $block = $block_manager->createInstance('deliverables', [
      'count' => 10,
      'add_on_empty' => TRUE,
      'more_link' => TRUE,
    ]);
    $build['deliverables'] = [
      '#type' => 'details',
      '#title' => t('Deliverables'),
      '#open' => TRUE,
      'content' => $block->build(),
    ];
  }
}

/**
 * Add document field to deliverable entities.
 */
function deliverable_add_document_field(DeliverableTypeInterface $type) {
  $field_storage = FieldStorageConfig::loadByName('deliverable', 'document');
  $field = FieldConfig::loadByName('deliverable', $type->id(), 'document');
  if (empty($field)) {
    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => $type->id(),
      'label' => t('Document'),
      'settings' => [
        'file_directory' => 'deliverable_document',
        'file_extensions' => 'jpg jpeg gif png txt xls pdf ppt pps odt ods odp gz tgz patch diff zip test info po pot psd yml mov mp4 avi mkv',
        'max_filesize' => '50 MB',
        'description_field' => FALSE,
        'handler' => 'default:file',
        'handler_settings' => [],
      ],
    ]);
    $field->save();
  }

}
