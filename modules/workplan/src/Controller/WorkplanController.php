<?php

namespace Drupal\workplan\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\cbo_project\ProjectInterface;
use Drupal\workplan\WorkplanInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns responses for Workplan routes.
 */
class WorkplanController extends ControllerBase {

  /**
   * Provides add project workplan form.
   */
  public function projectWorkplanAddForm(ProjectInterface $project) {
    $values = [
      'project' => $project->id(),
    ];
    if ($oid = $project->get('organization')->target_id) {
      $values['organization'] = $oid;
    }
    $workplan = $this->entityManager()->getStorage('workplan')
      ->create($values);

    $form = $this->entityFormBuilder()->getForm($workplan);

    return $form;
  }

  /**
   * Apply workflow transition.
   */
  public function applyTransition(Request $request, WorkplanInterface $workplan, $transition_id) {
    /** @var \Drupal\state_machine\Plugin\Field\FieldType\StateItem $state_item */
    $state_item = $workplan->get('status')->first();
    $transitions = $state_item->getTransitions();
    if ($transition = $transitions[$transition_id]) {
      $state_item->applyTransition($transition);
      $workplan->save();
    }

    if ($destination = $request->query->get('destination')) {
      return new RedirectResponse($destination);
    }
    else {
      return new RedirectResponse($workplan->toUrl()->toString());
    }
  }

}
