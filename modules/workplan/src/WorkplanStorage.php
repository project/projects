<?php

namespace Drupal\workplan;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * Defines the storage handler class for workplans.
 */
class WorkplanStorage extends SqlContentEntityStorage implements WorkplanStorageInterface {

}
