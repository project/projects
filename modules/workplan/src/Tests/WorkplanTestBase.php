<?php

namespace Drupal\workplan\Tests;

use Drupal\cbo_project\Tests\ProjectTestBase;
use Drupal\workplan\Entity\Workplan;

/**
 * Provides tool functions.
 */
abstract class WorkplanTestBase extends ProjectTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['workplan'];

  /**
   * A workplan.
   *
   * @var \Drupal\workplan\WorkplanInterface
   */
  protected $workplan;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->workplan = $this->createWorkplan([
      'project' => $this->project->id(),
    ]);
  }

  /**
   * Creates a workplan based on default settings.
   */
  protected function createWorkplan(array $settings = []) {
    // Populate defaults array.
    $settings += [
      'id' => strtolower($this->randomMachineName(8)),
      'label' => $this->randomMachineName(8),
    ];
    $entity = Workplan::create($settings);
    $entity->save();

    return $entity;
  }

}
