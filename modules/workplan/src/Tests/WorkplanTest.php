<?php

namespace Drupal\workplan\Tests;

/**
 * Tests workplan entities.
 *
 * @group workplan
 */
class WorkplanTest extends WorkplanTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  public static $modules = ['block', 'views'];

  /**
   * Tests the project workplans page, add, save.
   */
  public function testProjectWorkplan() {
    $this->drupalPlaceBlock('local_tasks_block');
    $this->drupalPlaceBlock('local_actions_block');

    $this->drupalLogin($this->projectManagerUser);

    $this->drupalGet('admin/project/' . $this->project->id());
    $this->assertLinkByHref('admin/project/' . $this->project->id() . '/workplan');

    $this->clickLink(t('Workplans'));
    $this->assertResponse(200);
    $this->assertLinkByHref('admin/project/' . $this->project->id() . '/workplan/add');

    $this->clickLink(t('Add workplan'));
    $this->assertResponse(200);

    $edit = [
      'title[0][value]' => $this->randomMachineName(8),
      'number[0][value]' => $this->randomMachineName(8),
    ];
    $this->drupalPostForm(NULL, $edit, t('Save'));
    $this->assertResponse(200);
    $this->assertText($edit['title[0][value]']);
  }

  /**
   * Tests the edit form.
   */
  public function testEdit() {
    $this->drupalPlaceBlock('local_tasks_block');

    $this->drupalLogin($this->projectManagerUser);

    $this->drupalGet('admin/workplan/' . $this->workplan->id());
    $this->assertResponse(200);
    $this->assertLinkByHref('admin/workplan/' . $this->workplan->id() . '/edit');

    $this->clickLink(t('Edit'));
    $this->assertResponse(200);

    $edit = [
      'title[0][value]' => $this->randomMachineName(8),
    ];
    $this->drupalPostForm(NULL, $edit, t('Save'));
    $this->assertResponse(200);
    $this->assertText($edit['title[0][value]']);
  }

}
