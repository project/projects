<?php

namespace Drupal\workplan;

use Drupal\Core\Entity\ContentEntityStorageInterface;

/**
 * Defines an interface for workplan entity storage classes.
 */
interface WorkplanStorageInterface extends ContentEntityStorageInterface {

}
