<?php

namespace Drupal\workplan;

use Drupal\views\EntityViewsData;

/**
 * Provides the views data for the workplan entity types.
 */
class WorkplanViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['workplan']['bulk_form'] = [
      'title' => $this->t('Operations bulk form'),
      'help' => $this->t('Add a form element that lets you run operations on multiple workplans.'),
      'field' => [
        'id' => 'bulk_form',
      ],
    ];

    $data['workplan']['project']['argument']['id'] = 'project';
    $data['workplan']['status']['filter']['id'] = 'state_machine_state';
    // $data['workplan']['progress_status']['filter']['id'] = 'list_field';

    return $data;
  }

}
