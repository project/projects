<?php

namespace Drupal\workplan;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the workplan entity type.
 *
 * @see \Drupal\workplan\Entity\Workplan
 */
class WorkplanAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $workplan, $operation, AccountInterface $account) {
    if ($operation == 'update' && $workplan->get('status')->value != 'working') {
      return AccessResult::forbidden('Workplan editing are now allowed in status other than the "working" status.');
    }

    /** @var \Drupal\cbo_project\ProjectInterface $project */
    if ($project = $workplan->get('project')->entity) {
      switch ($operation) {
        case 'view':
        case 'view label':
        case 'update':
          return $project->access($operation, $account, TRUE);

        case 'delete':
          return $project->access('update', $account, TRUE);
      }
    }

    return parent::checkAccess($workplan, $operation, $account);
  }

}
