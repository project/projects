<?php

namespace Drupal\workplan;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a workplan entity.
 */
interface WorkplanInterface extends ContentEntityInterface, EntityChangedInterface {

}
