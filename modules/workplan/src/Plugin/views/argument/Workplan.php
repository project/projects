<?php

namespace Drupal\workplan\Plugin\views\argument;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\workplan\WorkplanStorageInterface;
use Drupal\views\Plugin\views\argument\NumericArgument;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Argument handler for workplan pid.
 *
 * @ingroup views_argument_handlers
 *
 * @ViewsArgument("workplan")
 */
class Workplan extends NumericArgument implements ContainerFactoryPluginInterface {

  /**
   * Workplan storage handler.
   *
   * @var \Drupal\workplan\WorkplanStorageInterface
   */
  protected $workplanStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, WorkplanStorageInterface $workplan_storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->workplanStorage = $workplan_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity.manager')->getStorage('workplan')
    );
  }

  /**
   * Override the behavior of title(). Get the title of the workplan.
   */
  public function title() {
    // There might be no valid argument.
    if ($this->argument) {
      $workplan = $this->workplanStorage->load($this->argument);
      if (!empty($workplan)) {
        return $workplan->label();
      }
    }
    return $this->t('No name');
  }

}
