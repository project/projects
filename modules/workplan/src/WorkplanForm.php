<?php

namespace Drupal\workplan;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form handler for the workplan edit forms.
 */
class WorkplanForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $workplan = $this->entity;
    $insert = $workplan->isNew();
    $workplan->save();
    $workplan_link = $workplan->link($this->t('View'));
    $context = ['%title' => $workplan->label(), 'link' => $workplan_link];
    $t_args = ['%title' => $workplan->link($workplan->label())];

    if ($insert) {
      $this->logger('workplan')->notice('Workplan: added %title.', $context);
      drupal_set_message($this->t('Workplan %title has been created.', $t_args));
    }
    else {
      $this->logger('workplan')->notice('Workplan: updated %title.', $context);
      drupal_set_message($this->t('Workplan %title has been updated.', $t_args));
    }
  }

}
