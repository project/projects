<?php

namespace Drupal\cbo_project;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\people\PeopleManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler for the project edit forms.
 */
class ProjectForm extends ContentEntityForm {

  /**
   * The people manager service.
   *
   * @var \Drupal\people\PeopleManagerInterface
   */
  protected $peopleManager;

  /**
   * Constructs a ProjectForm object.
   *
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager.
   * @param \Drupal\people\PeopleManagerInterface $people_manager
   *   The people manager.
   */
  public function __construct(EntityManagerInterface $entity_manager, PeopleManagerInterface $people_manager) {
    parent::__construct($entity_manager);

    $this->peopleManager = $people_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager'),
      $container->get('people.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareEntity() {
    /** @var \Drupal\cbo_project\ProjectInterface $project */
    $project = $this->entity;
    if ($project->isNew()) {
      if (!$project->organization->target_id) {
        if ($organization = $this->peopleManager->currentOrganization()) {
          $project->organization->target_id = $organization->id();
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form['advanced'] = [
      '#type' => 'vertical_tabs',
      '#attributes' => ['class' => ['entity-meta']],
      '#weight' => 99,
    ];
    $form = parent::form($form, $form_state);

    $form['dates'] = [
      '#type' => 'details',
      '#group' => 'advanced',
      '#title' => $this->t('Dates'),
    ];

    if (isset($form['actual_dates'])) {
      $form['actual_dates']['#group'] = 'dates';
    }
    if (isset($form['schedule_dates'])) {
      $form['schedule_dates']['#group'] = 'dates';
    }

    if (isset($form['resource_lists'])) {
      $form['resources'] = [
        '#type' => 'details',
        '#group' => 'advanced',
        '#title' => $this->t('Resources'),
      ];

      $form['resource_lists']['#group'] = 'resources';
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $project = $this->entity;
    $insert = $project->isNew();
    $project->save();
    $project_link = $project->link($this->t('View'));
    $context = ['%title' => $project->label(), 'link' => $project_link];
    $t_args = ['%title' => $project->link($project->label())];

    if ($insert) {
      $this->logger('project')->notice('Project: added %title.', $context);
      drupal_set_message($this->t('Project %title has been created.', $t_args));
    }
    else {
      $this->logger('project')->notice('@type: updated %title.', $context);
      drupal_set_message($this->t('Project %title has been updated.', $t_args));
    }
  }

}
