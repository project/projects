<?php

namespace Drupal\cbo_project\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\RendererInterface;
use Drupal\cbo_organization\OrganizationInterface;
use Drupal\cbo_project\ProjectTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Project routes.
 */
class ProjectController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a NodeController object.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   */
  public function __construct(RendererInterface $renderer) {
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('renderer')
    );
  }

  /**
   * Provides the organization project form.
   */
  public function organizationProjectAddPage(OrganizationInterface $organization) {
    $bundles = $this->entityManager()->getBundleInfo('project');
    $build = [
      '#theme' => 'entity_add_list',
      '#bundles' => [],
    ];

    $link_text = $this->t('Add a new project type.');
    $build['#add_bundle_message'] = $this->t('There is no @entity_type yet. @add_link', [
      '@entity_type' => $this->t('project type'),
      '@add_link' => Link::createFromRoute($link_text, 'entity.project_type.add_form')->toString(),
    ]);
    // Filter out the bundles the user doesn't have access to.
    $access_control_handler = $this->entityTypeManager()->getAccessControlHandler('project');
    foreach ($bundles as $bundle_name => $bundle_info) {
      $access = $access_control_handler->createAccess($bundle_name, NULL, [], TRUE);
      if (!$access->isAllowed()) {
        unset($bundles[$bundle_name]);
      }
      $this->renderer->addCacheableDependency($build, $access);
    }

    // Redirect if there's only one bundle available.
    if (count($bundles) == 1) {
      $bundle_names = array_keys($bundles);
      return $this->redirect('project.organization_project.add_form', [
        'organization' => $organization->id(),
        'project_type' => reset($bundle_names),
      ]);
    }
    // Prepare the #bundles array for the template.
    foreach ($bundles as $bundle_name => $bundle_info) {
      $build['#bundles'][$bundle_name] = [
        'label' => $bundle_info['label'],
        'description' => isset($bundle_info['description']) ? $bundle_info['description'] : '',
        'add_link' => Link::createFromRoute(
          $bundle_info['label'],
          'project.organization_project.add_form',
          [
            'organization' => $organization->id(),
            'project_type' => $bundle_name,
          ]
        ),
      ];
    }

    return $build;
  }

  /**
   * Provides add organization project form.
   */
  public function organizationProjectAddForm(OrganizationInterface $organization, ProjectTypeInterface $project_type) {
    $project = $this->entityManager()->getStorage('project')->create([
      'organization' => $organization->id(),
      'type' => $project_type->id(),
    ]);

    $form = $this->entityFormBuilder()->getForm($project);

    return $form;
  }

}
