<?php

namespace Drupal\cbo_project\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting a project.
 */
class ProjectDeleteForm extends ContentEntityDeleteForm {

}
