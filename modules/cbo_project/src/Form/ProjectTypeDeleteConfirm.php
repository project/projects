<?php

namespace Drupal\cbo_project\Form;

use Drupal\Core\Entity\EntityDeleteForm;

/**
 * Provides a form for project type deletion.
 */
class ProjectTypeDeleteConfirm extends EntityDeleteForm {

}
