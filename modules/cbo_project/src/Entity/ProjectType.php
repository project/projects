<?php

namespace Drupal\cbo_project\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\cbo_project\ProjectTypeInterface;

/**
 * Defines the Project type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "project_type",
 *   label = @Translation("Project type"),
 *   handlers = {
 *     "access" = "Drupal\cbo_project\ProjectTypeAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\cbo_project\ProjectTypeForm",
 *       "delete" = "Drupal\cbo_project\Form\ProjectTypeDeleteConfirm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\cbo_project\ProjectTypeListBuilder",
 *   },
 *   admin_permission = "administer project types",
 *   config_prefix = "type",
 *   bundle_of = "project",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   links = {
 *     "add-form" = "/admin/project/type/add",
 *     "edit-form" = "/admin/project/type/{project_type}/edit",
 *     "delete-form" = "/admin/project/type/{project_type}/delete",
 *     "collection" = "/admin/project/type",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *   }
 * )
 */
class ProjectType extends ConfigEntityBundleBase implements ProjectTypeInterface {

  /**
   * The machine name of this project type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the project type.
   *
   * @var string
   */
  protected $label;

  /**
   * A brief description of this project type.
   *
   * @var string
   */
  protected $description;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

}
