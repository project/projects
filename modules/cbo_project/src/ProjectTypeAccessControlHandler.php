<?php

namespace Drupal\cbo_project;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the project type entity type.
 *
 * @see \Drupal\cbo_project\Entity\ProjectType
 */
class ProjectTypeAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    if ($operation == 'view') {
      return AccessResult::allowedIfHasPermission($account, 'access project');
    }

    return parent::checkAccess($entity, $operation, $account);
  }

}
