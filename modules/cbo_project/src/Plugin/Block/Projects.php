<?php

namespace Drupal\cbo_project\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block to display recent projects.
 *
 * @Block(
 *   id = "projects",
 *   admin_label = @Translation("Projects")
 * )
 */
class Projects extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The currently active route match object.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a new SystemMenuBlock.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current active route match object.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, EntityTypeManagerInterface $entity_type_manager, RouteMatchInterface $route_match) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'count' => 0,
      'add_on_empty' => FALSE,
      'more_link' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->configuration;

    $form['count'] = [
      '#type' => 'number',
      '#title' => $this->t('Item count'),
      '#min' => 1,
      '#default_value' => $config['count'],
      '#description' => $this->t('The item count to display.'),
    ];

    $form['add_on_empty'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add on empty'),
      '#defalut_value' => $config['add_on_empty'],
    ];

    $form['more_link'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display more link'),
      '#default_value' => $config['more_link'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['count'] = $form_state->getValue('count');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->configuration;

    $projectStorage = $this->entityTypeManager->getStorage('project');
    $query = $projectStorage->getQuery();

    if ($organization = $this->routeMatch->getParameter('organization')) {
      $query->condition('organization', $organization->id());
    }

    if (!$config['count'] > 0) {
      $query->range(NULL, $config['count']);
    }
    $ids = $query->sort('changed', 'DESC')
      ->execute();

    $items = [];
    if ($ids) {
      $projects = $projectStorage->loadMultiple($ids);
      /** @var \Drupal\cbo_project\ProjectInterface $project */
      $items = array_map(function ($project) {
        return $project->toLink();
      }, $projects);
    }

    $build['list'] = [
      '#theme' => 'item_list',
      '#items' => $items,
      '#empty' => $this->t('No projects.'),
    ];
    if (empty($items) && $config['add_on_empty']) {
      if ($organization) {
        $build['list']['#empty'] = Link::createFromRoute($this->t('Add project'), 'project.organization_project.add_page', [
          'organization' => $organization->id(),
        ]);
      }
      else {
        $build['list']['#empty'] = Link::createFromRoute($this->t('Add project'), 'entity.project.add_page');
      }
    }

    if ($config['more_link']) {
      if ($organization) {
        $url = Url::fromRoute('project.organization_project', [
          'organization' => $organization->id(),
        ]);
      }
      else {
        $url = Url::fromRoute('entity.project.collection');
      }
      $build['more_link'] = [
        '#type' => 'more_link',
        '#url' => $url,
      ];
    }

    return $build;
  }

}
