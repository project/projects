<?php

namespace Drupal\cbo_project\Plugin\views\argument;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\cbo_project\ProjectStorageInterface;
use Drupal\views\Plugin\views\argument\NumericArgument;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Argument handler for project pid.
 *
 * @ingroup views_argument_handlers
 *
 * @ViewsArgument("project")
 */
class Project extends NumericArgument implements ContainerFactoryPluginInterface {

  /**
   * Project storage handler.
   *
   * @var \Drupal\cbo_project\ProjectStorageInterface
   */
  protected $projectStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ProjectStorageInterface $project_storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->projectStorage = $project_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity.manager')->getStorage('project')
    );
  }

  /**
   * Override the behavior of title(). Get the title of the project.
   */
  public function title() {
    // There might be no valid argument.
    if ($this->argument) {
      $project = $this->projectStorage->load($this->argument);
      if (!empty($project)) {
        return $project->label();
      }
    }
    return $this->t('No name');
  }

}
