<?php

namespace Drupal\cbo_project;

use Drupal\Core\Entity\ContentEntityStorageInterface;

/**
 * Defines an interface for project entity storage classes.
 */
interface ProjectStorageInterface extends ContentEntityStorageInterface {

}
