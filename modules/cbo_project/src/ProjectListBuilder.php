<?php

namespace Drupal\cbo_project;

use Drupal\Core\Entity\EntityListBuilder;

/**
 * Defines a class to build a listing of project entities.
 *
 * @see \Drupal\cbo_project\Entity\Project
 */
class ProjectListBuilder extends EntityListBuilder {

}
