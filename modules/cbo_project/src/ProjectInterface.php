<?php

namespace Drupal\cbo_project;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a project entity.
 */
interface ProjectInterface extends ContentEntityInterface {

  /**
   * Get the organization.
   *
   * @return \Drupal\cbo_organization\OrganizationInterface|null
   *   The organization.
   */
  public function getOrganization();

}
