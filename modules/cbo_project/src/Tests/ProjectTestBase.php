<?php

namespace Drupal\cbo_project\Tests;

use Drupal\people\Tests\PeopleTestBase;
use Drupal\cbo_project\Entity\Project;
use Drupal\cbo_project\Entity\ProjectType;

/**
 * Sets up testing project type and project.
 */
abstract class ProjectTestBase extends PeopleTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['cbo_project'];

  /**
   * A user with project manager role.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $projectManagerUser;

  /**
   * A project type.
   *
   * @var \Drupal\cbo_project\ProjectTypeInterface
   */
  protected $projectType;

  /**
   * A project.
   *
   * @var \Drupal\cbo_project\ProjectInterface
   */
  protected $project;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->projectManagerUser = $this->drupalCreateUser();
    $this->projectManagerUser->addRole('project_manager');
    $this->projectManagerUser->people->target_id = $this->people->id();
    $this->projectManagerUser->save();

    $this->projectType = $this->createProjectType();
    $this->project = $this->createProject($this->projectType->id());
  }

  /**
   * Creates a project type based on default settings.
   */
  protected function createProjectType(array $settings = []) {
    // Populate defaults array.
    $settings += [
      'id' => strtolower($this->randomMachineName(8)),
      'label' => $this->randomMachineName(8),
    ];
    $entity = ProjectType::create($settings);
    $entity->save();

    return $entity;
  }

  /**
   * Creates a project based on default settings.
   */
  protected function createProject($project_type, array $settings = []) {
    // Populate defaults array.
    $settings += [
      'type' => $project_type,
      'number' => $this->randomMachineName(8),
      'title' => $this->randomMachineName(8),
      'organization' => $this->organization->id(),
    ];
    $entity = Project::create($settings);
    $entity->save();

    return $entity;
  }

}
