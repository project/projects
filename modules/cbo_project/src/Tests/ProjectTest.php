<?php

namespace Drupal\cbo_project\Tests;

/**
 * Tests project entities.
 *
 * @group cbo_project
 */
class ProjectTest extends ProjectTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  public static $modules = ['block', 'views'];

  /**
   * Tests the list, add, save.
   */
  public function testList() {
    $this->drupalPlaceBlock('local_actions_block');

    $this->drupalLogin($this->projectManagerUser);

    $this->drupalGet('admin/project');
    $this->assertResponse(200);
    $this->assertLinkByHref('admin/project/add');

    $this->clickLink('Add project');
    $this->assertResponse(200);

    $edit = [
      'title[0][value]' => $this->randomMachineName(8),
      'number[0][value]' => $this->randomMachineName(8),
      'organization[0][target_id]' => $this->organization->label() . ' (' . $this->organization->id() . ')',
    ];
    $this->drupalPostForm(NULL, $edit, t('Save'));
    $this->assertResponse(200);
    $this->assertText($edit['title[0][value]']);
  }

  /**
   * Tests the edit form.
   */
  public function testEdit() {
    $this->drupalPlaceBlock('local_tasks_block');

    $this->drupalLogin($this->projectManagerUser);

    $this->drupalGet('admin/project/' . $this->project->id());
    $this->assertResponse(200);
    $this->assertLinkByHref('admin/project/' . $this->project->id() . '/edit');

    $this->clickLink(t('Edit'));
    $this->assertResponse(200);

    $edit = [
      'title[0][value]' => $this->randomMachineName(8),
    ];
    $this->drupalPostForm(NULL, $edit, t('Save'));
    $this->assertResponse(200);
    $this->assertText($edit['title[0][value]']);
  }

  /**
   * Tests organization project page.
   */
  public function testOrganization() {
    $this->drupalPlaceBlock('local_tasks_block');
    $this->drupalPlaceBlock('local_actions_block');

    $this->drupalLogin($this->projectManagerUser);

    $this->drupalGet('admin/organization/' . $this->organization->id());
    $this->assertLink($this->project->label(), 0, 'Projects displayed');
    $this->assertLinkByHref('admin/organization/' . $this->organization->id() . '/project');

    $this->clickLink(t('Projects'), 1);
    $this->assertResponse(200);
    $this->assertLinkByHref('admin/organization/' . $this->organization->id() . '/project/add');
  }

}
