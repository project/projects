<?php

namespace Drupal\cbo_project\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\cbo_project\ProjectInterface;
use Symfony\Component\Routing\Route;

/**
 * Determines access for project.
 */
class ProjectAccessCheck implements AccessInterface {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * Constructs a EntityCreateAccessCheck object.
   *
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager.
   */
  public function __construct(EntityManagerInterface $entity_manager) {
    $this->entityManager = $entity_manager;
  }

  /**
   * Checks access to the project operation on the given route.
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {
    $operation = $route->getRequirement('_project_access');
    if ($project = $route_match->getParameter('project')) {
      if (!is_object($project)) {
        $project = $this->entityManager->getStorage('project')->load($project);
      }
      if ($project instanceof ProjectInterface) {
        return $project->access($operation, $account, TRUE);
      }
    }
    return AccessResult::neutral();
  }

}
