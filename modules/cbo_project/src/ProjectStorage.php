<?php

namespace Drupal\cbo_project;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * Defines the storage handler class for projects.
 */
class ProjectStorage extends SqlContentEntityStorage implements ProjectStorageInterface {

}
