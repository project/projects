<?php

namespace Drupal\cbo_project;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * Provides an interface defining a project type entity.
 */
interface ProjectTypeInterface extends ConfigEntityInterface, EntityDescriptionInterface {

}
