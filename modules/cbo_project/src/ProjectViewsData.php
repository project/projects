<?php

namespace Drupal\cbo_project;

use Drupal\views\EntityViewsData;

/**
 * Provides the views data for the project entity type.
 */
class ProjectViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['project']['table']['base']['access query tag'] = 'project_access';

    $data['project']['organization']['argument']['id'] = 'organization';

    return $data;
  }

}
