<?php

/**
 * @file
 * Install, update and uninstall functions for the project module.
 */

use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Add 'number' and 'description' field to 'project' entities.
 */
function cbo_project_update_8201() {
  $storage_definition = BaseFieldDefinition::create('string')
    ->setLabel(t('Number'))
    ->setRequired(TRUE)
    ->setSetting('max_length', 255);

  \Drupal::entityDefinitionUpdateManager()
    ->installFieldStorageDefinition('number', 'project', 'project', $storage_definition);

  $storage_definition = BaseFieldDefinition::create('string')
    ->setLabel(t('Description'))
    ->setRequired(TRUE)
    ->setSetting('max_length', 255);

  \Drupal::entityDefinitionUpdateManager()
    ->installFieldStorageDefinition('description', 'project', 'project', $storage_definition);
}

/**
 * Add 'project_manager' field to 'project' entities.
 */
function cbo_project_update_8202() {
  $storage_definition = BaseFieldDefinition::create('entity_reference')
    ->setLabel(t('Project manager'))
    ->setSetting('target_type', 'people')
    ->setSetting('handler_settings', [
      'target_bundles' => ['employee' => 'employee'],
    ]);

  \Drupal::entityDefinitionUpdateManager()
    ->installFieldStorageDefinition('project_manager', 'project', 'project', $storage_definition);
}

/**
 * Add 'created' and 'changed' field to 'project' entities.
 */
function cbo_project_update_8203() {
  $storage_definition = BaseFieldDefinition::create('created')
    ->setLabel(t('Created'))
    ->setDescription(t('The timestamp that the project was created.'));

  \Drupal::entityDefinitionUpdateManager()
    ->installFieldStorageDefinition('created', 'project', 'project', $storage_definition);

  $storage_definition = BaseFieldDefinition::create('changed')
    ->setLabel(t('Changed'))
    ->setDescription(t('The timestamp that the project was last changed.'));

  \Drupal::entityDefinitionUpdateManager()
    ->installFieldStorageDefinition('changed', 'project', 'project', $storage_definition);
}

/**
 * Add the project_manager role.
 */
function cbo_project_update_8204() {
  $config_update = \Drupal::service('config_update.config_update');
  $config_update->import('user_role', 'project_manager');
}
