Drupal Projects

Project page: https://www.drupal.org/project/projects

Synopsis
--------
Drupal Projects provide a complete enterprise project management solution.
Give you a flexible approach to defining and managing your projects and the
people, schedules, deliverables, and finances associated with them.

Requirements
------------
business_core
config_update
migrate_source_csv
page_manager
panels
state_machine

Roadmap
-------
https://www.drupal.org/node/2846532

Dependencies
------------
Drupal 8.x

projects 8.x-2.x is for Drupal 8.2.x
projects 8.x-3.x is for Drupal 8.3.x

pareview.sh
-----------
https://pareview.sh/node/1513
