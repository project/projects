<?php

namespace Drupal\projects\EventSubscriber;

use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Provides role based home.
 */
class RoleBasedHome implements EventSubscriberInterface {

  /**
   * The path.matcher service.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructs a RoleBasedHome event subscriber.
   *
   * @param \Drupal\Core\Path\PathMatcherInterface $path_matcher
   *   The path.matcher service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(PathMatcherInterface $path_matcher, AccountInterface $current_user) {
    $this->pathMatcher = $path_matcher;
    $this->currentUser = $current_user;
  }

  /**
   * Redirect to role based home page.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *   The request event.
   */
  public function onRequest(GetResponseEvent $event) {
    if ($this->pathMatcher->isFrontPage()) {
      $roles = $this->currentUser->getRoles();
      if (in_array('supplier', $roles)) {
        $front = 'projects/supplier';
      }
      elseif (in_array('project_manager', $roles)) {
        $front = 'projects/project_manager';
      }
      elseif (in_array('employee', $roles)) {
        $front = 'projects/employee';
      }
      else {
        return;
      }

      $event->setResponse(new RedirectResponse($front));
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = 'onRequest';
    return $events;
  }

}
