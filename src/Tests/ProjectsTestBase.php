<?php

namespace Drupal\projects\Tests;

use Drupal\cbo_project\Tests\ProjectTestBase;

/**
 * Setup modules.
 */
abstract class ProjectsTestBase extends ProjectTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['projects'];

}
