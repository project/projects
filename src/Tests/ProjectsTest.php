<?php

namespace Drupal\projects\Tests;

/**
 * Tests projects module.
 *
 * @group projects
 */
class ProjectsTest extends ProjectsTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  public static $modules = ['block'];

  /**
   * Tests the Home page for project manager.
   */
  public function testProjectManagerHome() {
    $this->drupalPlaceBlock('projects_implementation_status');

    $this->drupalLogin($this->projectManagerUser);

    $this->drupalGet('<front>');
    $this->assertResponse(200);
    $this->assertLink($this->project->label());
    $this->assertLinkByHref('admin/projects/implementation_status');

    $this->clickLink(t('Projects Implementation Status'));
  }

  /**
   * Tests the Home page for employee.
   */
  public function testEmployeeHome() {
    $this->drupalLogin($this->employeeUser);

    $this->drupalGet('<front>');
    $this->assertResponse(200);
    $this->assertLink($this->project->label());
  }

}
