<?php

namespace Drupal\projects\Controller;

use Drupal\cbs\Controller\CbsControllerBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Url;

/**
 * Returns response for Projects routes.
 */
class ProjectsController extends CbsControllerBase {

  /**
   * Display the implementation status.
   */
  public function implementationStatus() {
    $items = [];

    $items[] = $this->checkEntity(count($items) + 1, $this->t('Define organizations'), 'organization');
    $items[] = $this->checkEntity(count($items) + 1, $this->t('Define job groups'), 'job_group');
    $items[] = $this->checkEntity(count($items) + 1, $this->t('Define jobs'), 'job');
    $items[] = $this->checkEntity(count($items) + 1, $this->t('Define people'), 'people');
    $items[] = $this->checkEntity(count($items) + 1, $this->t('Define task types'), 'task_type');
    $items[] = $this->checkEntity(count($items) + 1, $this->t('Define resource lists'), 'resource_list');
    $items[] = $this->checkEntity(count($items) + 1, $this->t('Define project types'), 'project_type');

    $items[] = [
      '#markup' => $this->t('Step @step <a href=":url">Remove the implementation status block</a>', [
        '@step' => count($items) + 1,
        ':url' => Url::fromRoute('block.admin_display')->toString(),
      ]),
      '#wrapper_attributes' => [
        'class' => [
          'not-finished',
        ],
      ],
    ];

    $output = parent::_implementationStatus($items);

    $tags = $this->entityTypeManager()->getDefinition('organization')
      ->getListCacheTags();
    $tags = Cache::mergeTags($tags,
      $this->entityTypeManager->getDefinition('job_group')
        ->getListCacheTags());
    $tags = Cache::mergeTags($tags,
      $this->entityTypeManager->getDefinition('job')
        ->getListCacheTags());
    $tags = Cache::mergeTags($tags,
      $this->entityTypeManager->getDefinition('people')
        ->getListCacheTags());
    $tags = Cache::mergeTags($tags,
      $this->entityTypeManager->getDefinition('task_type')
        ->getListCacheTags());
    $tags = Cache::mergeTags($tags,
      $this->entityTypeManager->getDefinition('resource_list')
        ->getListCacheTags());
    $tags = Cache::mergeTags($tags,
      $this->entityTypeManager->getDefinition('project_type')
        ->getListCacheTags());
    $output['#cache']['tags'] = $tags;

    return $output;
  }

}
